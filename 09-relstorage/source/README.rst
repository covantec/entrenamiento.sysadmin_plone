.. -*- coding: utf-8 -*-

RelStorage es una implementación de almacenamiento para ZODB que almacena pickles (objetos serializados) en
una base de datos relacional. PostgreSQL 8.1 y versiones posteriores (vía la librería psycopg2), MySQL
5.0.32+ / 5.1.34+ (vía la librería MySQLdb 1.2.2 y versiones posteriores), y Oracle 10g y 11g
(vía la librería cx_Oracle) son actualmente soportado. RelStorage remplazo el proyecto
PGStorage.

.. contents::

Características
===============

* Es un remplazo para FileStorage y ZEO.

* Hay una forma sencilla de convertir FileStorage a RelStorage y viceversa.
  También puede convertir una instancia RelStorage a una base de datos relacional diferente.

* Diseñado para los sitios de alto volumen: varias instancias ZODB pueden compartir la mismo
  base de datos. Esto es similar a ZEO, pero RelStorage no requiere ZEO.

* De acuerdo con algunas pruebas, RelStorage maneja alta concurrencia mejor que
  la combinación estándar de ZEO y FileStorage.

* Mientras FileStorage tarda más en iniciar como la base de datos crece debido a un
  índice de todos los objetos en memoria, RelStorage comienza rápidamente, independientemente de
  tamaño de la base de datos.

* Soporta undo, compactación, y blobs para ZODB basado en sistema de archivo.

* Tanto la historia de preservación y almacenamiento de histórico gratuito esta disponibles.

* Capaz de conmutación por error a bases de datos SQL replicadas.

* Libre, código abierto (ZPL 2.1)


Instalación
===========

Usted puede instalar RelStorage usando easy_install:

.. code-block:: sh

    # easy_install RelStorage

RelStorage requiere una versión de ZODB que es consciente de almacenamientos MVCC.
ZODB 3.9 soporta RelStorage sin parches. ZODB 3.7 y 3.8 puede
soporta RelStorage si se aplica un parche para ZODB. Usted puede obtener
versiones de ZODB con el parche ya aplicado aquí:

    http://packages.willowrise.org

Los parches también son incluidos en la distribución original de RelStorage.

Usted necesita el adaptador de base de datos Python que corresponda con su base de datos.
Instale ``psycopg2``, ``MySQLdb`` 1.2.2+, o ``cx_Oracle`` 4.3+.

Configurar su base de datos
---------------------------

Usted necesita configurar una base de datos y cuanta de usuario para RelStorage.
RelStorage rellenará la base de datos con su esquema de la primera vez
conecta.

PostgreSQL
~~~~~~~~~~

Si ha instalado PostgreSQL desde un paquete binario, es probable que tenga un
cuenta de usuario llamado ``postgres``. Desde PostgreSQL se respeta el nombre de
el usuario que ha iniciado sesión en forma predeterminada, cambiar la cuenta ``postgres``
para crear el usuario RelStorage y base de datos. Incluso el usuario ``root`` no tiene
los privilegios PostgreSQL que tiene la cuenta ``postgres``. Para
ejemplo:

.. code-block:: sh

    $ sudo su - postgres
    $ createuser --pwprompt zodbuser
    $ createdb -O zodbuser zodb

Las nuevas cuentas de PostgreSQL a menudo requieren modificaciones en el archivo ``pg_hba.conf``,
que contiene reglas de control de acceso basado en servidor. La ubicación del archivo 
``pg_hba.conf`` varía, pero ``/etc/postgresql/8.4/main/pg_hba.conf`` es
común. PostgreSQL procesa las reglas en orden, por lo que añadir nuevas reglas
antes de las reglas predeterminadas en lugar de después. He aquí una regla de ejemplo que
sólo permite conexiones locales por el usuario ``zodbuser`` a la 
base de datos ``zodb``::

    local  zodb  zodbuser  md5

PostgreSQL vuelve a leer el archivo ``pg_hba.conf`` cuando usted pregunta al volver a cargar ese
archivo de configuración:

.. code-block:: sh

    /etc/init.d/postgresql reload

MySQL
~~~~~

Utilice la utilidad ``mysql`` para crear la base de datos y cuenta de usuario. Nota
que por lo general se requiere la opción ``-p``. Debe utilizar la opción ``-p``
si la cuenta que desea acceder requiere una contraseña, pero
no debe usar la opción ``-p`` si la cuenta está accediendo no requiere una contraseña. 
Si usted no proporciona la opción ``-p``, y todavía la cuenta requiere una contraseña, 
la utilidad ``mysql`` no le sugerirán una contraseña y fallará al autenticar.

La mayoría de los usuarios puede iniciar la utilidad ``mysql`` con el siguiente
comando, usando cualquier cuenta de inicio de sesión:

.. code-block:: sh

    $ mysql -u root -p

Aquí algunos ejemplos de sentencias SQL para crear el usuario y base de datos:

.. code-block:: sql

    CREATE USER 'zodbuser'@'localhost' IDENTIFIED BY 'mypassword';
    CREATE DATABASE zodb;
    GRANT ALL ON zodb.* TO 'zodbuser'@'localhost';
    FLUSH PRIVILEGES;

Oracle
~~~~~~

La configuración inicial requerirá privilegios ``SYS``. El uso de Oracle 10g XE, usted
puede iniciar una sesión de ``SYS`` con los siguientes comandos shell:

.. code-block:: sh

    $ su - oracle
    $ sqlplus / as sysdba

Es necesario crear un usuario de base de datos y otorgar los privilegios de ejecución en
el paquete DBMS_LOCK a ese usuario.
Estas son algunas de las declaraciones de ejemplo SQL para crear la base de datos de usuario
y la concesión de los permisos necesarios:

.. code-block:: sql

    CREATE USER zodb IDENTIFIED BY mypassword;
    GRANT CONNECT, RESOURCE, CREATE TABLE, CREATE SEQUENCE TO zodb;
    GRANT EXECUTE ON DBMS_LOCK TO zodb;

Configurar Plone
----------------

Para instalar RelStorage en Plone, vea las instrucciones en el siguiente
articulo:

    http://shane.willowrise.com/archives/how-to-install-plone-with-relstorage-and-mysql/

Plone usa la receta ``plone.recipe.zope2instance`` Buildout para
generar el archivo zope.conf, entonces la forma mas fácil es configurar RelStorage en un
parámetro ``rel-storage`` en el archivo ``buildout.cfg``.
El parámetro ``rel-storage`` contiene opciones separadas por salto de lineas
con esos valores:

    * ``type``: cualquier de los tipos de base de datos soportados (``postgresql``, ``mysql``,
      o ``oracle``).
    
    * Las opciones RelStorage como ``cache-servers`` y ``poll-interval``

    * Las opciones especifica del Adaptador

Un ejemplo:

.. code-block:: cfg

    rel-storage =
        type mysql
        db plone
        user plone
        passwd PASSWORD

Configurando Zope 2
-------------------

Para integrare RelStorage en Zope 2, especifique un backend RelStorage 
en el archivo ``etc/zope.conf``. Remueva el principal punto de montaje y agregue uno de los 
siguientes bloques. Para PostgreSQL:

.. code-block:: xml

    %import relstorage
    <zodb_db main>
      mount-point /
      <relstorage>
        <postgresql>
          # The dsn is optional, as are each of the parameters in the dsn.
          dsn dbname='zodb' user='username' host='localhost' password='pass'
        </postgresql>
      </relstorage>
    </zodb_db>

Para MySQL:

.. code-block:: xml

    %import relstorage
    <zodb_db main>
      mount-point /
      <relstorage>
        <mysql>
          # Most of the options provided by MySQLdb are available.
          # See component.xml.
          db zodb
        </mysql>
      </relstorage>
    </zodb_db>

Para Oracle (10g XE en este ejemplo):

.. code-block:: xml

    %import relstorage
    <zodb_db main>
      mount-point /
      <relstorage>
        <oracle>
          user username
          password pass
          dsn XE
        </oracle>
     </relstorage>
    </zodb_db>

Agregue soporte a ZODB blob, provea una opción para el directorio blob que especifica
donde los blobs son almacenados. Por ejemplo:

.. code-block:: sh

    %import relstorage
    <zodb_db main>
      mount-point /
      <relstorage>
        blob-dir ./blobs
        <postgresql>
          dsn dbname='zodb' user='username' host='localhost' password='pass'
        </postgresql>
      </relstorage>
    </zodb_db>

Configurando ``repoze.zodbconn``
--------------------------------

Use RelStorage con ``repoze.zodbconn``, un paquete que hace disponible
la ZODB a aplicaciones WSGI, cree un archivo de configuración conteniendo 
algo similar como lo siguiente:

.. code-block:: xml

    %import relstorage
    <zodb main>
      <relstorage>
        <mysql>
          db zodb
        </mysql>
      </relstorage>
      cache-size 100000
    </zodb>

``repoze.zodbconn`` espera una URI ZODB.  Use una URI de la siguiente forma
``zconfig://path/to/configuration#main``.


Utilidades Incluidas
====================

``zodbconvert``
---------------

RelStorage viene con un script llamado ``zodbconvert`` que convertimos
las bases de datos entre formatos. Lo utilizan para convertir una instancia FileStorage a
RelStorage y viceversa, o para convertir entre diferentes tipos de casos
de instancias RelStorage, o para convertir otros tipos de almacenamientos que
soportar el protocolo iterador de almacenamiento.

Cuando la conversión entre dos bases de datos preservando el histórico (nótese que
FileStorage utiliza un formato de historia de preservación), el script ``zodbconvert``
conserva todos los objetos y las transacciones, lo que significa que aún puede utilizar el
ZODB deshacer característica después de la conversión, y se puede convertir de nuevo usando
el mismo proceso a la inversa. Cuando se pasa de una base de datos libre de historial
ya sea a una base de datos sin historial o bases de datos preservando el historial, 
el script ``zodbconvert`` conserva todos los datos, pero la convierte
transacciones no serán posible deshacerlo. Cuando se pasa de una base de datos
preservando el historial a una una base de datos sin historial, el script ``zodbconvert``
cae toda la información histórica durante la conversión.

¿Como usar ``zodbconvert``?
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Crear un archivo de configuración al estilo ZConfig que especifica dos almacenamiento,
uno llamado "source", el otro "destination". El formato del archivo de configuración
es muy parecido al archivo zope.conf. A continuación, ejecute el comando ``zodbconvert``,
proporcionando el nombre del archivo de configuración como un parámetro.

La utilidad no modifica el almacenamiento de origen (source). Antes de copiar la
datos, la utilidad verifica el almacenamiento de destino (destination) está completamente vacío.
Si el almacenamiento de destino (destination) no está vacía, la utilidad aborta sin
hacer cualquier cambio en el destino (destination). (Adición de transacciones a una
base de datos existente es complejo y fuera del alcance de comando ``zodbconvert``.)

Aquí un ejemplo de archivo de configuración ``zodbconvert``:

.. code-block:: xml

  <filestorage source>
    path /zope/var/Data.fs
  </filestorage>

  <relstorage destination>
    <mysql>
      db zodb
    </mysql>
  </relstorage>

Este archivo de configuración especifica que la utilidad debe copiar todos
las transacciones del archivo Data.fs a una base de datos MySQL llamados "zodb". Si
querer revertir la conversión, cambiar los nombres de "source" y
"destination". Todos los tipos de almacenamiento y opciones de almacenamiento disponibles en
el archivo zope.conf también están disponibles en este archivo de configuración.

Opciones para ``zodbconvert``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  ``--clear``
    Borra todos los datos del almacenamiento de destino antes de copiar. Usa
    esto sólo si está seguro de que en el destino tiene datos que no son útiles.
    Actualmente sólo funciona cuando el destino es una instancia RelStorage.

  ``--dry-run``
    Abre ambos almacenamientos y analiza lo que se copian, pero no lo hace
    en realidad copiar.


``zodbpack``
------------

RelStorage también viene con un script llamado ``zodbpack`` que compacta cualquier
almacenamiento ZODB que permite conexiones simultáneas (incluyendo RelStorage
y ZEO, pero sin incluir FileStorage). Use el script ``zodbpack`` en tareas o 
scripts ``cron``. Pase el script del nombre de un archivo de configuración que lista 
los almacenamientos para compactar, en formato ZConfig. Un ejemplo de archivo de configuración:

.. code-block:: xml

  <relstorage>
    pack-gc true
    <mysql>
      db zodb
    </mysql>
  </relstorage>

Opciones para ``zodbpack``
~~~~~~~~~~~~~~~~~~~~~~~~~~

  ``--days`` o ``-d``
    Especifica el número de días de datos históricos para mantener. El valor predeterminado es 0,
    es decir, no se mantiene histórico. Esto es significativo, incluso para
    almacenamientos libre de historial, ya que no se quitan los objetos sin referencia
    desde la base de datos hasta que el número especificado de días que han pasado.

  ``--prepack``
    Indica al almacenamiento que sólo se ejecutará la fase de pre-pack de la compactación, pero no
    borrar realmente nada. Esto es equivalente a especificar
    ``pack-prepack-only true`` en las opciones de almacenamiento.

  ``--use-prepack-state``
    Indica al almacenamiento que sólo se ejecutará la fase de eliminación (compactación), saltándose
    la fase de análisis pre-pack. Esto es equivalente a especificar
    ``pack-skip-prepack true`` en las opciones de almacenamiento.


Migrando a una nueva versión de RelStorage
==========================================

Algunas veces necesitas una modificación del esquema a lo largo de una 
actualización de software.  Con suerte, esto no suele ser necesario.

Migración a la versión 1.5 de RelStorage requiere una actualización de esquema.
Ver `migrate-to-1.5.txt`_.

.. _`migrate-to-1.5.txt`: http://svn.zope.org/*checkout*/relstorage/trunk/notes/migrate-to-1.5.txt

Migración a la versión 1.4.2 de RelStorage requiere una actualización de esquema si
usted usando un base de datos libre de historial (significa que el keep-history es false).
Ver `migrate-to-1.4.txt`_.

.. _`migrate-to-1.4.txt`: http://svn.zope.org/*checkout*/relstorage/trunk/notes/migrate-to-1.4.txt

Ver el `subdirectorio de notas`_ si usted esta actualizando desde una versión antigua.

.. _`subdirectorio de notas`: http://svn.zope.org/relstorage/trunk/notes/


Opciones de RelStorage
======================

Especifique estas opciones en el archivo zope.conf, como los parámetros para la
constructor ``relstorage.storage.RelStorage``, o como los atributos de una
instancia ``relstorage.options.Options`` . En los dos últimos casos, el uso
subraya en lugar de guiones en los nombres de las opciones.

``name``
        El nombre del almacenamiento. Por defecto es un nombre descriptivo que
        incluye los parámetros de conexión del adaptador, excepto la contraseña
        de base de datos.

``read-only``
        Si es verdad, sólo lee puede ser ejecutado en contra del almacenamiento.

``blob-dir``
        Si se suministra, el almacenamiento proporcionará soporta a Blob de ZODB; esta
        opción especifica el nombre del directorio para guardar datos de Blob.
        El directorio se creará si no existe. Si no hay ningún valor
        (o un valor vacío) se proporciona, entonces ningún soporte a Blob será
        proporcionado.

``shared-blob-dir``
        Si es true (por defecto), el directorio de blob se supone que es
        compartida entre todos los clientes que utilizan NFS o similar; los datos blob
        serán almacenados sólo en el sistema de archivos y no en la base de datos. Si
        es false, los datos blob se almacenara en la base de datos relacional y el
        directorio blob mantiene una caché de blobs. Cuando esta opción está
        falso, el directorio blob no debe ser compartida entre los clientes.

        Esta opción debe ser true cuando se utiliza ZODB 3.8, porque ZODB 3.8
        no es compatible con el formato de archivo requerido de un caché de 
        blob. Utilice ZODB 3.9 o posterior si desea almacenar blobs en
        la base de datos relacional.

``blob-cache-size``
        El tamaño máximo de la memoria caché blob, en bytes. Si está vacío (el
        predeterminado), el tamaño de la caché no está marcada y el directorio blob
        crecerá sin límites. Esto debería ser vacía o significativamente el más grande 
        blob que usted almacena. Al menos se recomienda 1 gigabyte para bases de datos
        típicas. Más es recomendado si almacena archivos grandes como videos, CD / DVD
        imágenes o imágenes de máquinas virtuales.

        Esta opción permite sufijos como "mb" o "gb".
        Esta opción se ignora si shared-blob-dir es true.

``blob-cache-size-check``
        Chequea el tamaño del caché de blob como porcentaje del blob-cache-size: "10" significa
        "10%". El tamaño de la caché blob se comprobará cuando esta cantidad de bytes
        se han cargado en la memoria caché. Por defecto es el 10% del tamaño de la caché
        blob.

        Esta opción se ignora si shared-blob-dir es true.

``blob-chunk-size``
        Cuando los blobs ZODB se guardan en MySQL, RelStorage les rompe en
        trozos para minimizar el impacto en la memoria RAM. Esta opción especifica el tamaño 
        del trozo de nuevos blobs. En PostgreSQL y Oracle, este valor se utiliza como
        el tamaño del búfer de memoria para las operaciones de carga y descarga blob. La
        omisión es 1048576 (1 mebibyte).

        Esta opción permite sufijos como "mb" o "gb".
        Esta opción se ignora si shared-blob-dir es true.

``keep-history``
        Si esta opción se establece en true (por defecto), el adaptador
        va a crear y utilizar un esquema de base de datos con historial
        (como FileStorage). Un esquema a preservar el historial soporta deshacer
        a nivel ZODB, pero también crece más rápidamente y requiere una amplia
        compactación sobre una base regular.

        Si esta opción se establece en false, el adaptador creará y
        utilizar un esquema de base de datos sin historial. Deshacer no contará con el soporte,
        pero la base de datos no crecerá tan rápidamente. La base de datos
        aún requieren la recolección regular de basura (que es accesible
        a través del mecanismo paquete de base de datos.)

        Esta opción no debe cambiar una vez que el esquema de base de datos 
        ha sido instalado, ya que los esquemas de almacenamiento con historial y
        sin historial son diferentes. Si desea convertir entre una base de datos con historial 
        y una base de datos sin historial, el uso la utilidad ``zodbconvert`` 
        para copiar a una nueva base de datos.

``replica-conf``
        Si no se proporciona esta opción, se especifica un archivo de texto que
        contiene una lista de base de datos de réplicas, que el adaptador puede elegir
        desde este archivo. Para MySQL y PostgreSQL, poner en el archivo de réplica una lista
        de valores ``host:port`` o ``host``, una línea por cada uno. Para Oracle,
        poner en una lista de valores de DSN. Las líneas en blanco y las líneas que empiezan
        con ``#`` son ignorados.

        El adaptador prefiere la primera réplica especificado en el archivo. Si
        el primero no está disponible, el adaptador intenta automáticamente la
        resto de las réplicas, en orden. Si el archivo cambia, el adaptador 
        caerá conexiones de base de datos SQL existente y hacer
        nuevas conexiones cuando ZODB inicia una nueva transacción.

``ro-replica-conf``
        Al igual que la opción ``replica-conf``, mas el archivo de texto referenciado
        proporciona una lista de réplicas de bases de datos a utilizar sólo para acceso de sólo lectura
        para conexiones cargadas. Esto le permite a RelStorage para cargar objetos desde las
        réplicas de bases de datos de sólo lectura, mientras que el uso de réplicas de lectura-escritura
        para todos los demás interacciones de base de datos.

        Si no se proporciona esta opción, las conexiones de carga recaerán
        a la pool réplica especificado por la opción ``replica-conf``. Si
        Se proporciona la opción ``ro-replica-conf`` pero la opción ``replica-conf`` no es,
        RelStorage utilizará las réplicas para conexiones cargadas, pero no para
        otras interacciones de bases de datos.

        Tenga en cuenta que si sólo las réplicas de lectura son asíncronas, la siguiente
        interacción después de una operación de escritura puede no estar actualizada.
        Cuando eso sucede, RelStorage registrará un "viaje en el tiempo hacia atrás"
        con advertencia y borrara la caché ZODB para evitar errores de coherencia.
        Esto probablemente se traducirá en una reducción temporal del performance de como
        la caché ZODB se repobló.

``replica-timeout``
        Si esta opción tiene un valor distinto de cero, cuando el adaptador selecciona
        una réplica distinta de la réplica principal, el adaptador
        tratar de volver a la réplica principal después del tiempo de espera
        especificada (en segundos). El valor predeterminado es de 600, lo que significa 10 minutos.

``revert-when-stale``
        Especifica qué hacer cuando una conexión de base de datos este rancia.
        Esto es especialmente aplicable a una bases de datos replicada de 
        forma asíncrona: RelStorage podría cambiar a una réplica que aún no
        está actualizada.

        Cuando la opción ``revert-when-stale`` es ``false`` (por defecto) y la
        conexión de base de datos esta rancia, RelStorage levantará un
        ReadConflictError si la aplicación intenta leer o escribir
        cualquier cosa. La aplicación debe reaccionar a la
        ReadConflictError por volver a intentar la operación después de un retraso
        (posiblemente varias veces.) Una vez que la base de datos se pone 
        al día, una transacción posterior verá la actualización y la
        ReadConflictError no ocurrirá de nuevo.

        Cuando la opción ``revert-when-stale`` es ``true`` y la conexión de base de datos
        está rancia, RelStorage registrará una advertencia, limpia la caché de las
        conexiones ZODB (para evitar errores de coherencia), y deja que
        la aplicación continúe con el estado base de datos desde
        una transacción anterior. Este comportamiento se pretende que sea útil
        para la alta disponibilidad, de los clientes ZODB de sólo lectura. La activación de esta
        opción en los clientes ZODB que leen y escriben la base de datos es
        pueda causar confusión para los usuarios cuyos cambios
        parecen revertirse temporalmente.

``poll-interval``
        Diferir el poll de la base de datos para el intervalo de tiempo máximo especificado,
        en segundos. Se establece en 0 (por defecto) para siempre poll. El fraccionario
        en segundos es permitido. Utilice esta opción para aligerar la carga de base de datos en
        servidores con alto volumen de lectura y bajo volumen de escritura.

        La opción poll-interval funciona mejor en combinación con
        la opción cache-servers. Si ambos están activados, RelStorage hará una 
        encuesta una la única clave de caché para los cambios en cada request.
        La base de datos no se encuestó a menos que el caché indica
        se han producido cambios, o el tiempo de espera especificado por la opción poll-interval
        ha expirado. Esta configuración mantiene a los clientes totalmente al día,
        mientras se quita la parte de la carga de polling desde la base de datos.
        Una buena configuración del clúster es usar servidores memcache
        y con un alto intervalo de polling (por ejemplo, 60 segundos).

        Esta opción se puede utilizar sin la opción cache-servers,
        pero un gran intervalo de polling sin cache-servers aumenta la
        probabilidad de basar las transacciones en datos obsoletos, que no lo hace
        afectar la coherencia de base de datos, pero no aumentará la probabilidad
        de errores de conflicto, dando lugar a un bajo rendimiento.

``pack-gc``
        Si la opción pack-gc es false, las operaciones de carga no realizara
        la recolección de basura. La recolección de basura está activado por defecto.

        Si la recolección de basura está desactivada, las operaciones de carga mantienen al menos una
        revisión de cada objeto. Con la recolección de basura desactivado, el
        código de paquete no tiene que seguir las referencias a objetos, por lo que la 
        compactación es concebible mucho más rápido. Sin embargo, parte de ese beneficio
        se pueden perder debido a un número cada vez mayor de objetos no utilizados.

        Desactivación de la recolección de basura es también un hack que asegura
        referencias entre bases de datos nunca se rompen.

``pack-prepack-only``
        Si la opción pack-prepack sólo es true, las operaciones de carga realizan un análisis completo
        al compactar, pero los datos no se quitan realmente. Después de una pre-pack,
        los pack_object, pack_state y las tablas pack_state_tid se llenan
        con la lista de estados de objetos y los objetos que habrían sido
        eliminado. Si la opción pack-gc es true, la tabla object_ref también será totalmente
        poblada. La tabla object_ref se puede consultar para descubrir referencias
        entre los objetos almacenados.

``pack-skip-prepack``
        Si la opción pack-skip-prepack es true, la fase de pre-pack se salta y se
        se supone las tablas pack_object, pack_state y pack_state_tid tienen
        ha llenado ya. Así la compactación sólo afectará a los registros ya
        objetivo para la compactación por un anterior análisis pre-pack a ejecutar.

        Utilice esta opción junto con pack-prepack-only para dividir la compactación en
        fases distintas, donde cada fase se puede ejecutar en diferentes
        intervalos de tiempo, o cuando un análisis pre-pack se ejecuta en una copia de la
        base de datos para aliviar una carga base de datos de producción.

``pack-batch-timeout``
        La compactación produce en lotes de transacciones; esto especifica el
        tiempo de espera en segundos para cada lote. Tenga en cuenta que algunas configuraciones 
        base de datos tienen un rendimiento impredecible de E/S
        y podría estancar mucho más largo que el tiempo de espera.
        El tiempo de espera predeterminado es de 1.0 segundos.

``pack-commit-busy-delay``
        Antes de cada lote de compactación, se solicita el bloqueo de commit. Si el bloqueo está
        ya posea una regular de commit, la compactación se realiza una pausa durante un corto
        tiempo. Esta opción especifica en el tiempo que debe ser el proceso de compactación es
        pausado antes de intentar conseguir de nuevo el bloqueo de commit.
        El retardo por defecto es de 5.0 segundos.

``cache-servers``
        Especifica una lista de servidores memcached. Usando memcached con
        RelStorage mejora la velocidad del objeto que se accede frecuente mientras
        reducir ligeramente la velocidad de otras operaciones.

        Proporcionar una lista de host: pares de puertos, separados por espacios en blanco.
        "127.0.0.1:11211" es un escenario común. Algunos módulos memcached,
        como pylibmc, le permiten especificar una ruta de acceso a un socket de Unix
        en lugar de un par host:port.

        El valor predeterminado es desactivar la integración memcached.

``cache-module-name``
        Especifica que módulo Python memcache a utilizar. El valor predeterminado es
        "relstorage.pylibmc_wrapper", que requiere el modulo pylibmc. Un
        módulo alternativo es "memcache", un módulo puro de Python. Si
        utiliza el módulo memcache, utilice al menos la versión 1.47. Esta
        opción no tiene efecto a menos que los cache-servers se establezca.

``cache-prefix``
        El prefijo para todas las claves en la memoria caché. Todos los clientes usan una
        base de datos que debe utilizar el mismo cache-prefix. Por defecto es el
        nombre de base de datos. (Por ejemplo, en PostgreSQL, el nombre de la base de datos
        se determina mediante la ejecución de ``SELECT current_database()``.)
        Establezca este si tiene varias bases de datos con el mismo nombre.

``cache-local-mb``
        RelStorage almacena en caché los objetos conservados pickle en la memoria, de 
        forma similar a un caché ZEO. La caché "local" es compartida entre los hilos. 
        Esta opción configura la máxima cantidad aproximada de memoria caché que
        deben consumir, en megabytes. Su valor predeterminado es 10. Defina a
        0 para desactivar la caché en memoria.

``cache-local-object-max``
        Esta opción configura el tamaño máximo de un objeto pickle
        (en bytes) que puede calificar para la caché "local". El tamaño es
        medido antes de la compresión. Los objetos más grandes aún pueden calificar
        para memcache. El valor predeterminado es 16384 (1 << 14) bytes.

``cache-local-compression``
        Esta opción configura la compresión dentro de la caché "local".
        Este módulo nombra un Python que proporciona las opciones de dos funciones,
        ``compress()`` y ``decompress()``. Los valores admitidos son
        ``zlib``, ``bz2``, y ``none`` (sin compresión). El valor predeterminado es
        ``zlib``.

``cache-delta-size-limit``
        Esta es una opción avanzada. RelStorage utiliza un sistema de
        checkpoints para mejorar la tasa de aciertos de caché. Esta opción
        configura cuántos objetos se debe almacenar antes de crear una
        nuevo checkpoints. El valor predeterminado es 10000.

``commit-lock-timeout``
        Durante el commit, RelStorage adquiere un bloqueo en toda la base de datos. Esta
        opción especifica el tiempo de espera para el bloqueo antes
        no el intento fallido de hacer commit. El valor predeterminado es 30 segundos.

        Los adaptadores de MySQL y Oracle admiten esta opción. la
        Adaptador de PostgreSQL no lo hace actualmente.

``commit-lock-id``
        Durante el commit, RelStorage adquiere un bloqueo en toda la base de datos. Esta
        opción especifica el ID de bloqueo. Esta opción se aplica en la actualidad
        sólo al adaptador de Oracle.

``create-schema``
        Normalmente, RelStorage creará o actualizar el esquema de base de datos en
        la puesta en marcha. Establezca esta opción en false si necesita conectarse a una
        base de datos RelStorage sin creación automática o actualizaciones.

Opciones de Adaptador
=====================

Opciones de Adaptador PostgreSQL
--------------------------------

El adaptador PostgreSQL acepta:

``dsn``
    Especifica el nombre del origen de datos para conectarse a PostgreSQL.
    Un DSN PostgreSQL es una lista de parámetros separados por
    espacio en blanco. Un DSN típico puede ser algo así::

        dbname='zodb' user='username' host='localhost' password='pass'

    Si se omite el DSN, el adaptador se conecta a una base de datos local sin
    contraseña. Tanto el nombre de usuario y una base de datos que coincida con la
    nombre del propietario del proceso actual.

Opciones de Adaptador MySQL
---------------------------

El adaptador MySQL acepta la mayoría de los parámetros suportados por la librería MySQL-python, 
incluyendo:

``host``
    string, host a conectar
``user``
    string, usuario para conectar
``passwd``
    string, contraseña a usar
``db``
    string, base de datos a usar
``port``
    integer, puerto TCP/IP a conectar
``unix_socket``
    string, ubicación de unix_socket (solamente para UNIX)
``conv``
    mapiando, mapea MySQL FIELD_TYPE.* a funciones Python el cual convierte una
    cadena a el apropiado tipo Python
``connect_timeout``
    número de segundos para esperar antes de fallar la conexión.
``compress``
    si se define, la compresión gzip es habilitados
``named_pipe``
    si se define, conecta al servidor vía named pipe (solamente Windows)
``init_command``
    comando el cual ejecuta una ves que la conexión es creada
``read_default_file``
    ver la documentación MySQL para mysql_options()
``read_default_group``
    ver la documentación MySQL para mysql_options()
``client_flag``
    bandera cliente desde MySQLdb.constants.CLIENT
``load_infile``
    int, non-zero habilita LOAD LOCAL INFILE, deshabilita zero

Opciones de Adaptador Oracle
----------------------------

El adaptador Oracle acepta:

``user``
        El nombre de cuenta Oracle
``password``
        La contraseña de la nombre de cuenta Oracle
``dsn``
        El nombre del origen de datos Oracle.  La librería cliente Oracle 
        normalmente espera para buscará el DSN en /etc/oratab.

Usar con zodburi
================

Este paquete también habilite el uso de esquemas de URI ``postgres://``, ``mysql://``
y ``oracle://`` para zodburi_.
Para más información sobre zodburi, por favor, consulte esa documentación. Esta 
sección contiene información especifica a esos esquemas.

.. _zodburi: https://pypi.org/project/zodburi

URI schemes
--------------------------

Los esquemas de URI ``postgres://`` , ``mysql://`` y ``oracle://`` pueden 
ser pasado como ``zodbconn.uri`` para crear una base de datos RelStorage PostgresSQL,
MySQL o Oracle. La uri debería contener el usuario,
la contraseña, el host, el puerto y el nombre de la base de datos e.j.::

  postgres://someuser:somepass@somehost:5432/somedb?connection_cache_size=20000
  mysql://someuser:somepass@somehost:5432/somedb?connection_cache_size=20000

Debido a que la información de conexión de Oracle están más a menudo dada como un DSN, la
dirección URI de Oracle no deben contener la misma información que la otra, pero
sólo el DSN ::

  oracle://?dsn="HERE GOES THE DSN"

Los esquemas de URI también acepta argumentos de cadenas consultas. Los argumentos de cadenas
consultas honorado por este esquema que siguen.

RelStorage-constructor related
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Esos argumentos generalmente informa al constructor de RelStorage acerca 
de los valores de los mismos nombres :

poll_interval, cache_local_mb, commit_lock_timeout, commit_lock_id,
read_only, shared_blob_dir, keep_history, pack_gc, pack_dry_run,
strict_tpc, create, blob_cache_size, blob_cache_size_check,
blob_cache_chunk_size, replica_timeout, pack_batch_timeout,
pack_duty_cycle, pack_max_delay, name, blob_dir, replica_conf,
cache_module_name, cache_prefix, cache_delta_size_limit, cache_servers

Usual zodburi arguments
~~~~~~~~~~~~~~~~~~~~~~~

Los argumentos que son usualmente con zodburi están también disponibles aquí (ver
http://docs.pylonsproject.org/projects/zodburi/en/latest/) :

demostorage
  boolean (si es true, envuelve RelStorage en un DemoStorage)
database_name
  string
connection_cache_size
  integer (por defecto 10000)
connection_pool_size
  integer (por defecto 7)

especifico para Postgres
~~~~~~~~~~~~~~~~~~~~~~~~

connection_timeout
  integer
ssl_mode
  string

especifico para Mysql
~~~~~~~~~~~~~~~~~~~~~

connection_timeout
  integer
client_flag
  integer
load_infile
  integer
compress
  boolean
named_pipe
  boolean
unix_socket
  string
init_command
  string
read_default_file
  string
read_default_group
  string

especifico para Oracle 
~~~~~~~~~~~~~~~~~~~~~~

twophase
  integer
user
  string
password
  string
dsn
  string

Ejemplo
~~~~~~~

Un ejemplo que combinan una ruta con una cadena de consulta::

  postgres://someuser:somepass@somehost:5432/somedb?connection_cache_size=20000

Desarrollo
==========

Usted puede comprobar una copia desde Subversion usando el siguiente comando:

.. code-block:: sh

    git clone https://github.com/zodb/relstorage.git

Usted también puede navegar el código:

    https://github.com/zodb/relstorage/

El mejor sitio para discutir sobre el desarrollo de RelStorage es en la lista de correo 
electrónico **zodb-dev**.



FAQs
====

P: ¿Cómo puedo ayudar a mejorar RelStorage?

    R: La mejor manera de ayudar es para probar y para proporcionar experiencias específicas 
    de la base de datos. Haga las preguntas sobre RelStorage en la lista de correo zodb-dev.

P: ¿Puedo realizar consultas SQL sobre los datos en la base de datos?

    R: No. Como FileStorage y DirectoryStorage, RelStorage almacena los datos
    como objetos pickles Python, por lo que es difícil para cualquier cosa, a menos para la ZODB que interpretar los datos. 
    Un proyecto anterior llamado Ape trató de almacenar datos en una manera de verdaderamente 
    relacional, pero resultó que Ape trabajó demasiado contra los principios de ZODB
    por tanto, no se podría hacer lo suficiente fiable para su uso en producción. RelStorage,
    Por otro lado, es mucho más cerca de un almacenamiento ZODB ordinario, y es
    por lo tanto más apropiado para su uso en producción.

P: ¿Cómo se compara el desempeño RelStorage con FileStorage?

    A: De acuerdo con los puntos de referencia, RelStorage con PostgreSQL es a menudo más rápido que
    FileStorage, especialmente en condiciones de alta concurrencia.

P: ¿Por qué debería elegir RelStorage?

    R: Porque RelStorage es una capa bastante pequeña que se basa en las bases de datos de 
    clase mundial. Estas bases de datos han demostrado la fiabilidad y escalabilidad, a lo largo
    con numerosas opciones de soporte.

P: ¿Puede RelStorage reemplazar ZRS (Zope Replication Services)?

    R: Sí, RelStorage hereda el asíncrona de la replicación maestro/esclavo
    de capacidad de MySQL y otras bases de datos. RelStorage también ha sido
    demostrado que funciona con Oracle RAC.

P: ¿Cómo puedo configurar un entorno para ejecutar las pruebas RelStorage?

    A: Ver README.txt en el directorio relstorage/tests.

Direcciones URLs del proyecto
=============================

* https://pypi.org/project/RelStorage       (publicaciones PyPI y descargas)
* http://shane.willowrise.com/                 (blog)


Referencia
==========

* `Relstorage <https://raw.githubusercontent.com/zodb/relstorage/master/README.txt>`_
