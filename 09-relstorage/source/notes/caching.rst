.. -*- coding: utf-8 -*-

Cacheo con checkpoints
----------------------

La estrategia de Memcache incluye checkpoints. La administración del checkpoint es
un poco complejo, pero importante para lograr una tasa de aciertos de caché decente.

Los checkpoints son ID de transacción de 64 bits enteros. Invariante: checkpoint0
es mayor o igual que el checkpoint1, lo que significa que si son
diferente, checkpoint0 es el más reciente.

La clave de caché "$prefix:checkpoints" contiene los checkpoints actuales (0 y 1).
Si falta la clave de caché, configúrela con el tid actual, lo que significa
checkpoint1 y checkpoint0 están en el mismo punto.

Cada instancia de StorageCache contiene un mapa de Python de cambios en {oid: tid}
después del checkpoint0. Este mapa se llama delta_after0. El mapa
no se compartirá porque cada instancia actualiza el mapa en
tiempos diferentes.

La lista (oid, tid) recuperada del polling es suficiente para la actualización
delta_after0 directamente, a menos que checkpoint0 se haya movido desde la última encuesta.
Tenga en cuenta que delta_after0 podría tener un tid más reciente que los datos
proporcionado por polling, debido a la resolución de conflictos. La combinación
debería usar el último tid de cada mapa.

También mantenga un mapa de {oid: tid} cambios después del checkpoint1 y antes
o en el checkpoint0. Se llama delta_after1. Este mapa es
inmutable, por lo que sería bueno compartirlo entre hilos.

Al buscar un objeto en la memoria caché, intente obtener:

    - El estado en delta_after0.

    - El estado en el checkpoint0.

    - El estado en delta_after1.

    - El estado en el checkpoint1.

    - El estado de la base de datos.

    Si el estado recuperado es anterior al checkpoint0, pero
    no se recuperó de checkpoint0, lo guardó en el checkpoint0.
    Por lo tanto, si obtenemos datos de delta_after1 o checkpoint1, deberíamos
    copiarlo al checkpoint0.

La hora actual es ignorada; solo nos importa la transacción marcas de tiempo.
En cierto sentido, el tiempo se congela hasta la próxima transacción
a commit. Esto debería tener un efecto secundario de crear bases de datos que no
cambio a menudo extremadamente cacheable.

Después del polling, verifique la cantidad de objetos que se encuentran ahora en delta_after0. Si
está más allá de un umbral (quizás 10k), sugiere que las encuestas futuras usen
nuevos checkpoints. Actualizar "$prefix:checkpoints".

Los valores del checkpoint permanecen constantes dentro de una transacción. Incluso si los
la transacción lleva horas y sus datos son obsoletos, debe seguir intentándolo
recuperar de las tids especificadas en delta_after(0|1) y
checkpoint(0|1); puede continuar y almacenar en caché lo que recupera. Quien
sabe, podría haber otra transacción de larga ejecución que podría
usa los datos en caché

Si cargamos objetos polling, no use el caché.

Durante el polling, es posible que checkpoint0 sea mayor que el
última ID de transacción recién polling, ya que otras transacciones pueden ser
agregando datos muy rápidamente. Si eso sucede, la instancia debería
ignore la actualización del checkpoint, con la expectativa de que el nuevo checkpoint
será visible después de la próxima actualización.


