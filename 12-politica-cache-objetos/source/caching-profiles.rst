.. -*- coding: utf-8 -*-

Perfiles de caché
-----------------

Toda la configuración persistente para la maquinaria de almacenamiento en caché se almacena en el
panel del control Registro de configuración, que es gestionado por paquete ``plone.app.registry``.
Esto puede ser modificado mediante el paso de importación GenericSetup en el archivo ``registry.xml``.
En la pestaña *Importar configuraciones* del panel de control le permite importar estos perfiles de caché.

Perfiles de caché por defecto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El paquete ``plone.app.caching`` incluye tres perfiles de caché por defecto. Dos de estos
perfiles encapsulan la configuración de caché que se sabe que funcionan bien con una
instalación Plone por defecto típica. El tercero es un perfil de ejemplo para una
configuración de almacenamiento en caché "split-view" (ver la discusión de split-view esta
dividida posteriormente en este documento).

Los tres perfiles de caché por defecto:

* **Sin configuraciones de acelerador web**
      Configuraciones útiles para ambientes que no utilicen un acelerador web como Squid o Varnish.

* **Con configuraciones de acelerador web**
      Configuraciones útiles para ambientes que utilicen un acelerador web
      como Squid o Varnish. La única diferencia con el perfil "Sin configuraciones de acelerador web"
      son algunas opciones que habilitan el acelerador web para archivos/imágenes
      en de espacio de contenido y de los feeds de contenido.

* **Con configuraciones de acelerador web (y almacenamiento en caché con vista dividida)**
      Un perfil de ejemplo para una configuración de acelerador web con vista dividida en
      el almacenamiento en caché habilitado. En este ejemplo requiere una configuración especial proxy.
      Vea los ejemplos de proxy en el directorio "proxy-configs".


Perfiles de caché personalizados
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las políticas de caché son a menudo un compromiso entre velocidad y frescura.
El almacenamiento en caché más agresivo a menudo viene a costa de un mayor riesgo de
respuestas rancias. Los perfiles predeterminados proporcionados tienden a errar por el lado
de frescura sobre la velocidad por lo que hay un cierto margen para ajustar si es mayor
el deseo de la velocidad.

También puede ser necesaria la personalización si productos de terceros que están instalados
requieren un tratamiento especial. Examinar las cabeceras de respuesta HTTP para determinar
si el producto de terceros requiere un tratamiento especial. La mayoría de los casos sencillos
probablemente puede ser resuelto mediante la adición de un mapeo al tipo de contenido o 
plantilla apropiada. Los casos más complicados, pueden requerir operaciones de almacenamiento en caché 
personalizado.

Un perfil GenericSetup utilizado para el almacenamiento en caché debe estar registrado para el
interfaz marcador ``ICacheProfiles`` para distinguirla de los perfiles más generalmente
utilizados para instalar un producto. Esto también oculta el perfil de
panel de control Complementos de Plone.

Aquí hay un ejemplo de este paquete:

.. code-block:: xml
    
    <genericsetup:registerProfile
        name="with-caching-proxy"
        title="With caching proxy"
        description="Settings useful for setups with a caching proxy such as Squid or Varnish"
        directory="profiles/with-caching-proxy"
        provides="Products.GenericSetup.interfaces.EXTENSION"
        for="plone.app.caching.interfaces.ICacheProfiles"
        />

El directorio ``profiles/with-caching-proxy`` contiene un solo paso de importación,
el archivo ``registry.xml``, contiene los ajustes para configurar el conjunto de reglas de operación
mapeos, y opciones de ajuste para diversas operaciones. En el momento de la escritura,
esto incluye:

.. code-block:: xml
    
    <record name="plone.caching.interfaces.ICacheSettings.operationMapping">
        <value purge="False">
            <element key="plone.resource">plone.app.caching.strongCaching</element>
            <element key="plone.stableResource">plone.app.caching.strongCaching</element>
            <element key="plone.content.itemView">plone.app.caching.weakCaching</element>
            <element key="plone.content.feed">plone.app.caching.moderateCaching</element>
            <element key="plone.content.folderView">plone.app.caching.weakCaching</element>
            <element key="plone.content.file">plone.app.caching.moderateCaching</element>
        </value>
    </record>

Las opciones por defecto para las distintas operaciones estándar se encuentran en el
archivo ``registry.xml`` que es parte del perfil de instalación estándar para
este producto, en el directorio ``profiles/default``. El perfil personalizado
anula una serie de ajustes de operación de conjuntos de reglas específicas (ver abajo).
Por ejemplo:

.. code-block:: xml
    
    <record name="plone.app.caching.weakCaching.plone.content.itemView.ramCache">
        <field ref="plone.app.caching.weakCaching.ramCache" />
        <value>True</value>
    </record>

Esto permite el almacenamiento en caché de RAM para la operación "caché débil" para obtener recursos mediante
el conjunto de reglas ``plone.content.itemView``. El valor por defecto se define en el principal
archivo ``registry.xml`` como:

.. code-block:: xml
    
    <record name="plone.app.caching.weakCaching.ramCache">
        <field type="plone.registry.field.Bool">
            <title>RAM cache</title>
            <description>Turn on caching in Zope memory</description>
            <required>False</required>
        </field>
        <value>False</value>
    </record>

Observe cómo utilizamos un *field reference* para evitar tener que volver a definir el campo.

Puede ser útil mirar estos incluidos en el archivo ``registry.xml`` en busca de inspiración si
usted está construyendo su propio perfil de almacenamiento en caché. Como alternativa, puede exportar el
registro de la herramienta ``portal_setup`` y sacar los registros bajo el
prefijos ``plone.caching`` y ``plone.app.caching``.

Normalmente, el archivo ``registry.xml`` es todo lo que se requiere, pero usted es libre de añadir
los pasos de importación adicional si es necesario. También puede agregar un archivo ``metadata.xml``
y utilizar el mecanismo de la dependencia GenericSetup instalar otros perfiles al
vuelo.

