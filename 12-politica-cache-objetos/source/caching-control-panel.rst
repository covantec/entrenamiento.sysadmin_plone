.. -*- coding: utf-8 -*-

El panel control de caché
-------------------------

Después de la instalación, encontrará un panel de control *Caché* en el sitio de Plone. Este consta de cuatro pestañas principales:

* *Cambiar las configuraciones*, donde se puede controlar el comportamiento
  de almacenamiento en caché.

* *Importar configuraciones*, donde se puede importar configuraciones perfiles
  predefinidos de caché.

* *Purga proxy caché*, donde se puede purgar manualmente el contenido de un
  almacenamiento en caché proxy. Esta pestaña sólo aparece si tiene la opción
  *Habilitar purga* permitido bajo *Cambiar las configuraciones*.

* *Caché RAM*, donde se puede ver estadísticas sobre y que purgar la caché de RAM.

En la pestaña configuración, se encuentran cuatro conjuntos de campo:

* *Configuraciones globales*, para las opciones globales, tales como activa el almacenamiento en caché o desactivarlo.

* *Aceleradores web*, donde se puede controlar el uso de Plone con un proxy caché 
  como Squid o Varnish.

* *Operaciones de caché*, donde los conjuntos de reglas de almacenamiento en caché (pistas acerca de views y
   recursos utilizados para los propósitos de almacenamiento en caché) pueden estar asociados con operaciones del 
   almacenamiento en caché (que o bien interceptan una request para devolver una respuesta en caché, o
   modifica un response a agregar los encabezados de control de caché). Aquí es también donde los 
   conjuntos de reglas para las page templates legado (creadas a través de la Web o de la
   herramienta portal_skins) están configurados.

* *Configuraciones detalladas*, donde se pueden configurar los parámetros de operaciones de
  almacenamiento en caché individuales.

