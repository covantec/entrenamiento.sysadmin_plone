.. -*- coding: utf-8 -*-

Introducción
============

``plone.app.caching`` provee una interfaz de usuario Plone UI y reglas por defecto para administrar HTTP response cacheadas en Plone. Este paquete incorpora ``z3c.caching``, ``plone.caching`` y ``plone.cachepurging``.

``plone.app.caching`` requiere Plone 4 versiones superiores.


Instalación
===========

De Plone 4.1 en adelante, incorpora el paquete ``plone.app.caching`` como una dependencia de Plone. Está disponible desde el panel de control, pero no está activada de forma predeterminada. Usted puede activar la opción ``Soporte a Caché HTTP`` desde la opción del el panel de control"Complementos".

Instalación en Plone 4.0
-------------------------

Para instalar el paquete ``plone.app.caching``, agregarlo a la lista de ``eggs`` en su archivo ``buildout.cfg``, o como una dependencia de uno de sus propios paquetes en ``setup.py``. La configuración ZCML se cargará automáticamente a través de un entry point ``z3c.autoinclude``. Usted también necesitará instalar el paquete en el panel de control de Complementos de Plone.

Este paquete depende de un número de otros paquetes, incluyendo ``z3c.form`` y ``plone.app.registry``, que no se incluyen con Plone.
Es probable que desee para bloquear las versiones de los paquetes utilizando un buen conjunto conocido de versiones. Agregue esto a la línea de la ``extends`` en su archivo ``buildout.cfg``, *después* de la línea que incluye los KGS de Plone::

    extends =
        ...
        http://good-py.appspot.com/release/plone.app.caching/1.0a1

Actualizar el número de versión en el final de la URL según sea apropiado. Usted puede encontrar una visión general de las versiones
`aquí <http://good-py.appspot.com/release/plone.app.caching>`_.

