.. -*- coding: utf-8 -*-

.. Política de almacenamiento de cache documentation master file, created by
   sphinx-quickstart on Sun Aug 24 21:56:27 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===================================
Política de almacenamiento de caché
===================================

.. toctree::
    :maxdepth: 2
 
    README
    caching-control-panel
    caching-profiles
    rulesets-and-caching-operations
    caching-proxies
    ram-cache
    etags
    composite-views
    split-views

