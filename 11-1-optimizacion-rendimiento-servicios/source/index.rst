.. -*- coding: utf-8 -*-

.. Optimización del rendimiento del servicio documentation master file, created by
   sphinx-quickstart on Sun Aug 24 21:56:27 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Optimización del rendimiento del servicio
=========================================

.. toctree::
   :maxdepth: 2

   introduccion
   instalacion/index
   configuraciones
   arquitectura/index
   mantenimiento
   actualizacion
   depuracion
