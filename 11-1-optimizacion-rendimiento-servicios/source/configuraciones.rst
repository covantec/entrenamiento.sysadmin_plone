.. -*- coding: utf-8 -*-

.. highlight:: rest

.. _configuraciones_generales:

Configuraciones generales
=========================

Plone 4 ofrece varios niveles de configuración a continuación se describen cada uno:

.. _configuraciones_buildout:

Configuraciones Buildout
------------------------

A continuación se describen las configuraciones de zc.buildout obtenidas tras la descarga 
previa realizada en la sección llamada :ref:`Archivos y directorios obtenidos <archivos_directorios_obtenidos>`:

.. glossary::

   ``profiles/base.cfg``
      Esta configuración zc.buildout define las extensiones básicas para la construcción 
      del proyecto, además de la inclusión del perfil de instalación básico de Plone 4.3.4.

   ``profiles/base-versions.cfg``
      Esta configuración zc.buildout se extiende de la configuración ``base.cfg``, la cual 
      define el conjunto de versiones de paquetes a usar para la construcción.

   ``profiles/plone-4.3.4.cfg``
      Esta configuración zc.buildout define como construir un Plone 4.3.4.

   ``profiles/plone-4.3.4-versions.cfg``
      Esta configuración zc.buildout se extiende de la configuración ``plone-4.3.4.cfg``,
      la cual define el conjunto de versiones de paquetes a usar para la construcción.s

   ``profiles/hotfix.cfg``
      Esta configuración zc.buildout define los paquetes y las versiones actualizaciones 
      de seguridad de la Plone.

   ``profiles/development.cfg``
      Esta configuración zc.buildout define todas las tareas para construir y ensamblar 
      la Plone con el conjunto de herramientas para entornos de desarrollo.

   ``profiles/development-versions.cfg``
      Esta configuración zc.buildout se extiende de la configuración ``development.cfg``,
      la cual define el conjunto de versiones de paquetes a usar para la construcción.

   ``profiles/production.cfg``
      Esta configuración zc.buildout define todas las tareas para construir y ensamblar 
      la Plone con el conjunto de servicios necesario para que esta funcione bajo un 
      esquema de alta disponibilidad.

   ``profiles/production-kgv.cfg``
      Esta configuración zc.buildout se extiende de la configuración ``production.cfg``,
      la cual define el conjunto de versiones de paquetes a usar para la construcción.

   ``profiles/munin-install.cfg``
      Esta configuración zc.buildout genera las configuraciones de los plugins del servicio 
      :ref:`Munin <que_es_munin>` para los servicios la Plone.

   .. _profile_munin_zope:

   ``profiles/munin-zope.cfg``
      Esta configuración zc.buildout genera los plugins de :ref:`Munin <que_es_munin>` para 
      monitorear varios aspectos de Zope usando el paquete ``munin.zope``.
      
   ``profiles/munin-plone.cfg``
      Esta configuración zc.buildout genera un plugin de :ref:`Munin <que_es_munin>` para 
      monitorear varios aspectos de Plone usando el paquete ``munin.plone``.

   ``profiles/munin-haproxy.cfg``
      Esta configuración zc.buildout genera un plugin de :ref:`Munin <que_es_munin>` llamado ``haproxy_backend`` 
      para monitorear mínimamente :ref:`HAProxy <que_es_haproxy>`.

   ``profiles/munin-varnish.cfg``
      Esta configuración zc.buildout genera los plugins de :ref:`Munin <que_es_munin>` para monitorear varios
      aspectos de Zope usando el recipe ``munin.varnish`` para :ref:`Varnish <que_es_varnish>`.

      Este genera los siguientes scripts:

      - **varnish_mycompany_expunge**, analiza los objetos purgados.

      - **varnish_mycompany_transfer_rates**, analiza las tasas de transferencia.

      - **varnish_mycompany_objects**, analiza números de objetos en los encabezados.

      - **varnish_mycompany_uptime**, analiza el tiempo de funcionamiento al aire del servicio.

      - **varnish_mycompany_request_rate**, analiza las tasas de peticiones.

      - **varnish_mycompany_memory_usage**, analiza el uso de memoria.

      - **varnish_mycompany_hit_rate**, analiza las tasas de Hit.

      - **varnish_mycompany_threads**, analiza el estatus de Thread.

      - **varnish_mycompany_backend_traffic**, analiza el trafico del Backend.

   ``profiles/munin-nginx.cfg``
      Esta configuración zc.buildout genera un plugin de :ref:`Munin <que_es_munin>` llamado ``nginx_memory`` 
      para monitorear el consumo de memoria de :ref:`Nginx <nginx_setup>`.

   ``profiles/sources.cfg``
      Esta configuración zc.buildout define los recursos de paquetes eggs en desarrollo 
      que se agregaran al proyecto usando la extensión de zc.buildout llamada ``mr.developer``.

   ``profiles/staging.cfg``
      Esta configuración zc.buildout le permite ejecutar el proceso de construcción de Plone 
      paso a paso para depurar posibles fallas en el proceso de construcción de la herramienta.

   ``profiles/maintenance.cfg``
      Esta configuración zc.buildout le permite definir tareas de actualización y mantenimiento de Plone.

   ``profiles/buildout.cfg.example``
      Esta configuración zc.buildout ofrece una plantilla del archivo buildout.cfg que puede usar 
      para la construcción del proyecto Plone.

----

.. _configuraciones_generadas:

Configuraciones generadas
-------------------------

A continuación se describen las configuraciones para los servicios como :ref:`Nginx <nginx_setup>`,
:ref:`Varnish <que_es_varnish>`, :ref:`HAProxy <que_es_haproxy>`, tareas de mantenimiento entre otros
más que fueron generadas tras la construcción del proyecto realizada en la sección llamada 
:ref:`Inicie la construcción <inicio_construccion>`:

.. glossary::
    :sorted:

    ``etc/nginx.conf``
        Contiene las configuraciones para el servicio :ref:`Nginx <nginx_setup>`.

    ``etc/nginx-vhost.conf``
        Contiene las configuraciones para el virtual host de :ref:`Nginx <nginx_setup>`.

    ``etc/varnish.vcl``
        Contiene las configuraciones para el servicio :ref:`Varnish <que_es_varnish>`.
        
    ``etc/haproxy.conf``
        Contiene las configuraciones para el servicio :ref:`HAProxy <que_es_haproxy>`.

    ``etc/logrotate.conf``
        Contiene las configuraciones para rotar los archivos .log usando la herramienta ``logrotate``.

    ``etc/munin-plugin-mycompany.conf``
        Contiene las configuraciones de los plugins de :ref:`Munin <que_es_munin>` para Plone.
