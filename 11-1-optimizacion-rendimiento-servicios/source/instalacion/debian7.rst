.. -*- coding: utf-8 -*-

.. highlight:: rest

.. _debian7:

Debian GNU/Linux 7
==================

Para instalar Plone 4.3.x en entornos de servidores usando el sistema operativo 
`Debian GNU/Linux 7`_, debe ejecutar los siguientes pasos con sus respectivos 
comandos a continuación:

.. _upgrade_base:

Actualizaciones de seguridad
----------------------------

Primero, instale las últimas actualizaciones del *Debian GNU/Linux 7*, con el 
siguiente comando:

.. code-block:: bash

    $ sudo apt-get update
    $ sudo apt-get upgrade

.. _requerimientos_base:

Requerimientos base
-------------------
Se requiere instalar una serie de dependencias para el proceso de compilación, instalación 
y configuración de algunas dependencias de Plone 4.3.x, para esto debe ejecutar los 
siguientes comandos:

.. code-block:: bash

    $ sudo apt-get install gcc g++ make tar unzip bzip2 libssl-dev libxml2-dev \
                   zlib1g-dev libjpeg62-dev libreadline6-dev readline-common wv \
                   xpdf-utils python2.7-dev libxslt1-dev

.. _requerimientos_instalacion_git:

Instalación de Git
------------------
Se requiere instalar el control de versiones Git, para esto debe ejecutar el siguiente comando:

.. code-block:: bash
 
    $ sudo apt-get install git-core

.. _requerimientos_instalacion_nginx:

Instalación de Nginx
--------------------
Se requiere instalar el :ref:`servidor Web Nginx <nginx_setup>`, para esto debe ejecutar 
el siguiente comando:

.. code-block:: console
 
    $ sudo apt-get install nginx-extras

.. _requerimientos_instalacion_consumo_recursos:

Herramientas de monitoreo
-------------------------
Se requiere instalar algunas utilidades para el monitoreo de :ref:`consumo de recursos <consumo_recursos>`, 
para esto debe ejecutar el siguiente comando:

.. code-block:: console
 
    $ sudo apt-get install iotop htop

.. _requerimientos_instalacion_munin:

Instalación de Munin
--------------------
Se requiere instalar el :ref:`servicio de monitoreo Munin <munin_setup>`, para esto debe 
ejecutar el siguiente comando:

.. code-block:: console
 
    $ sudo apt-get install munin munin-node
    $ sudo apt-get install curl socat coreutils libwww-perl #Haproxy munin plugins

.. _Debian GNU/Linux 7: https://es.wikipedia.org/wiki/Debian_GNU/Linux
