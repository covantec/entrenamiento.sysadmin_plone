.. -*- coding: utf-8 -*-

.. highlight:: rest

.. _productos_adicionales:

=====================
Productos adicionales
=====================

Tras el proceso de :ref:`instalación de Plone <instalacion_website>`, se instalaron 
varios módulos y productos que permiten el funcionamiento de Plone los cuales se 
describen a continuación:

.. glossary::
   :sorted:

   ``Products.PloneFormGen``
      Este es un producto Plone le permite a los usuarios crear formulario de forma sencilla, agregando tipos de contenido, los cuales contiene todos los campos y acciones del formulario son totalmente configurables a través de la Web.

      .. tip::
         Más información acerca del producto visite la dirección https://pypi.org/project/Products.PloneFormGen/

.. warning::
    Todos los productos instalados se están ubicados en el :ref:`directorio que fue creado <archivos_directorios_construidos>` tras la construcción de Plone, este es llamado ``eggs/``.

----  

Recursos del desarrollador
==========================

Serie de recursos útiles al desarrollador

.. toctree::
   :maxdepth: 2

   Manual de referencia Git <http://git-scm.com/book/es/v2>
   
Enlaces de utilidad
-------------------

* Repositorio de código fuente: https://github.com/collective

.. _pdb: https://docs.python.org/release/2.4/lib/module-pdb.html
