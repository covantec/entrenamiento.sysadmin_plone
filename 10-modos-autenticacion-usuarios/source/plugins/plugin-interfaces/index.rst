====================
Interfaces de Plugin 
====================

.. admonition:: Descripción

        Plugins de PAS se desglosan por las diferentes funcionalidades que proporcionan.

.. toctree::
   :maxdepth: 2

   list-of-plugin-interfaces
