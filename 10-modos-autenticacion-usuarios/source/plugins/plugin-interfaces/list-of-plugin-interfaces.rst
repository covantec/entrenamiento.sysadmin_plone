=============================
Lista de Interfaces de Plugin
=============================

.. contents :: :local:

.. admonition:: Descripción

        Los plugins de PAS se desglosan por las diferentes funcionalidades que proporcionan. Un plugin particular puede proporcionar una o varias de las siguientes interfaces

* :ref:`pas-extraction-plugins`

  Los plugins de extracción son responsables de la extracción de las credenciales de la solicitud.

* :ref:`pas-authentication-plugins`

  Los plugins de autenticación son responsables de validar las credenciales generadas por el plugin de extracción.

* :ref:`pas-challenge-plugins`

  Los plugins reto inician un desafío para que el usuario cuando proporcione credenciales.

* :ref:`pas-update-credentials-plugins`

  Los plugins de actualización de credenciales responden al usuario cambiar las credenciales.

* :ref:`pas-reset-credentials-plugins`

  Los plugins de credenciales responden a un usuario salir de la sesión.

* Userfactory Plugins

  Crea usuarios.

* Anonymoususerfactory Plugins

  Crear usuarios anónimos.

* :ref:`pas-properties-plugins`

  Los plugins Properties generan hojas de propiedades para los usuarios.

* :ref:`pas-groups-plugins`

  Los plugins Groups determinar los grupos a los que pertenece un usuario.

* :ref:`pas-roles-plugins`

  Los plugins Roles determinan los roles globales que tiene un usuario.

* Update Plugins

  Los plugins Actualizar permiten que el usuario o la aplicación actualicen las propiedades del usuario.

* Validation Plugins

  Los plugins de validación especifican los valores permitidos para las propiedades de usuario (por ejemplo, la longitud mínima de la contraseña, los caracteres permitidos, etc.)

* :ref:`pas-user-enumeration-plugins`
.
  Los plugins de enumeración permiten consultar los usuarios por ID, y la búsqueda de los usuarios que coincidan con determinados criterios.

* :ref:`pas-user-adder-plugins`

  Los plugins de User Adder permiten al Pluggable Auth Service crear usuarios.

* :ref:`pas-group-enumeration-plugins`

  Los plugins de enumeración permiten consultar los grupos por ID.

* :ref:`pas-role-enumeration-plugins`

  Los plugins de enumeración permiten consultar los roles por ID.

* :ref:`pas-role-assigner-plugins`

  Los plugins de Role Assigner permiten al Pluggable Auth Service para asignar