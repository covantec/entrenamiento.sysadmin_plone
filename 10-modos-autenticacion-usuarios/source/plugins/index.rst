=======
Plugins
=======

.. admonition:: Descripción

        Detalle de los plugins de existencias previstas por el PAS y como crear otros nuevos

.. toctree::
   :maxdepth: 2

   plugin-interfaces/index
   plugin-types/index
