=====================================
Configurando un plugin PAS individual
=====================================

.. contents:: :local:

.. admonition:: Descripción

    Además de habilitar y deshabilitar los plugins a través del objeto plugins
    cada plugin también puede tener su propia configuración.
    Puede acceder a esta abriendo un plugin en el ZMI.


Tomando el ``credentials_cookie_auth`` como ejemplo de nuevo usted verá el
pantalla para el :guilabel:`Activate`.
Esta pestaña es obligatoria y le permite activar y desactivar las interfaces PAS
para un plugin.
Esto corresponde a la configuración del plugin que vimos antes, pero no
le permiten cambiar el orden de los diferentes plugins para una interfaz.
Si habilita una nueva interfaz para un plugin en particular, será
activado y se coloca la última en la lista de plugins para un interfaz
particular.

.. image:: cookie-plugin.jpg
   :width: 400 px
   :alt: cookie-plugin.jpg

También puede ir a la pestaña :guilabel:`properties` para editar la configuración
específico para este plugin:

.. image:: cookie-plugin-properties.jpg
   :width: 400 px
   :alt: cookie-plugin-properties.jpg

Lo que usted puede configurar diferirá por plugin. Algunos plugins no tienen
ninguna opciones de configuraciones, otros pueden ser muy complejos.