================
Creación de User
================

.. contents:: :local:

.. admonition:: Descripción

    PAS uses un algoritmo multifásico crear un objeto user

#. Un plugin ``IUserFactoryPlugin`` se utiliza para crear un nuevo objeto de user.
#. Todos los plugins ``IPropertiesPlugin`` se consultan para obtener las hojas de propiedades.
#. Todos los plugins ``IGroupsPlugin`` se consultan para obtener los grupos.
#. Todos los plugins ``IRolesPlugin`` se consultan para obtener los roles globales.
