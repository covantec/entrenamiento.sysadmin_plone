=========================
La interfaces importantes
=========================

.. contents:: :local:

.. admonition:: Descripción

    PAS tiene un número de interfaces que son importantes para todos.

La más interfaces importantes que usted podría querer configurar son:

**Authentication**
   Los plugins de autenticación son responsables de la autenticación de un conjunto de
   credenciales. Por lo general, eso significa verificar si un nombre de usuario y
   contraseña son correctos, comparándolos con un registro de usuario en una base de datos
   tales como el ZODB o una base de datos SQL.

**Extraction**
   Los plugins de extracción determinar las credenciales de una petición.
   Las credenciales pueden adoptar diferentes formas, tales como una cookie HTTP, formulario HTTP
   de datos o la dirección IP del usuario.

**Groups**
   Estos plugins determinar a qué grupo(s) de un usuario (o grupo) pertenece.

**Properties**
   Los plugins de Propiedad administra todas las propiedades de los usuarios.
   Esto incluye la información estándar tales como el nombre del usuario y
   dirección de correo electrónico, pero también puede ser cualquier otro dato que desea
   almacenar para un usuario.
   Los plugins múltiples de propiedades se pueden utilizar en paralelo,
   por lo que es posible por ejemplo usar cierta información de un sistema central
   como el directorio activo mientras que el almacenamiento de datos específicos de su
   sitio Plone en la ZODB.

**User Enumeration**
   Los plugins de enumeración de usuario implementan la lógica de búsqueda de los usuarios.
