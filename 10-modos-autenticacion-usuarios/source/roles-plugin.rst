===============
Plugin de Roles
===============

.. contents:: :local:

Los plugins ``IRolesPlugin`` determinan los roles globales de un principal.
Al igual que las otras interfaces de la interfaz ``IRolesPlugin`` contiene sólo una
solo método::

    def getRolesForPrincipal( principal, request=None ):
        """ principal -> ( role_1, ... role_N )
        o Return a sequence of role names which the principal has.
        o May assign roles based on values in the REQUEST object, if present.
        """

Aquí hay un ejemplo simple::

    def getRolesForPrincipal(self, principal, request=None):
        # Only act on the current user
        if getSecurityManager().getUser().getId()!=principal:
            return ()

        # Only act if the request originates from the local host
        if request is not None:
            ip=request.get("HTTP_X_FORWARDED_FOR", request.get("REMOTE_ADDR", ""))
            if ip!="127.0.0.1":
                return ()

        return ("Manager",)

Esto le da al usuario actual en rol de Manager si se está accediendo al sitio
desde el servidor Zope sí mismo.
