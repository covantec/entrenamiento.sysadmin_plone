================
Plugins de Group
================

.. contents:: :local:

Los Plugins de Group devuelven los id para los grupos de un principal es un
miembro. Puesto que un principal puede ser un usuario o un grupo significa esto
que el PAS puede soportar a los miembros de grupos anidados. La configuración por defecto PAS
no es compatible con este embargo.

Al igual que otras interfaces de PAS, la interfaz ``IGroupsPlugin`` es simple y
sólo especifica un único método::

    def getGroupsForPrincipal(principal, request=None):
        """ principal -> ( group_1, ... group_N )
        o Return a sequence of group names to which the principal
          (either a user or another group) belongs.
        o May assign groups based on values in the REQUEST object, if present
        """

Aquí hay un ejemplo simple::

    def getGroupsForPrincipal(self, principal, request=None):
        # Manager can not be itself
        if principal == "Manager":
            return ()

        # Only act on the current user
        if getSecurityManager().getUser().getId() != principal:
            return ()

        # Only act if the request originates from the local host
        if request is not None:
            ip=request.get("HTTP_X_FORWARDED_FOR", request.get("REMOTE_ADDR", ""))
            if ip != "127.0.0.1":
                return ()

        return ("Manager",)

Esto pone al usuario actual en el grupo Administrador si el sitio está siendo
accedido desde el servidor Zope sí mismo.
