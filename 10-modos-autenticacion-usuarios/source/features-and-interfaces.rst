============================
Características y interfaces
============================

.. contents:: :local:

.. admonition:: Descripción

    Una carpeta user, tales como proporciona PAS un número de diferentes servicios:
    se encarga de la autenticación de usuario,
    se pregunta a los usuarios al acceder, si es necesario,
    y que le permite buscar usuarios y grupos.

Con el fin de hacer tanto la configuración e implementación más simple
y más poderoso, todas estas tareas se han dividido
en diferentes interfaces.
Cada interfaz describe cómo una característica específica,
tales como la autenticación de un usuario, tiene que ser implementado.

Dentro de PAS, los plugins se utilizan para proporcionar estas características.
Los plugins son pequeñas piezas de la lógica que aplican una o más funciones
como se define por estas interfaces.

Esta separación es útil por diferentes razones:

* Hace que sea posible configurar diferentes aspectos del sistema
  por separado.
  Por ejemplo, *cómo* los usuarios se autentican (cookies, formularios de ingreso, etc.) pueden ser
  configurado por separado
  De *donde* la información del usuario se almacena (ZODB, LDAP, RADIUS, SQL, etc.).
  Esta flexibilidad hace que sea muy fácil de ajustar el sistema a las necesidades específicas.
* Hace posible a los desarrolladores escribir pequeños trozos de código
  que sólo realizar una única tarea.
  Esto conduce a código que es más fácil de entender,
  más comprobable y mejor mantenimiento.