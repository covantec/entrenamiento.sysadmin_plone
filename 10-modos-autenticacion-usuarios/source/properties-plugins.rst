=====================
Plugins de Properties
=====================

.. contents:: :local:

Las propiedades se almacenan en las hojas de propiedades:
Los objetos mapeados, como un diccionario Python estándar,
que contienen las propiedades de un principal.
Las hojas de propiedades se ordenan:
si una propiedad está presente en múltiples hojas de propiedades sólo la propiedad en
la hoja con la prioridad más alta es visible.

Las hojas de propiedades son creados por plugins que implementan la
interfaz ``IPropertiesPlugin``.
Esta interfaz contiene un solo método::

    def getPropertiesForUser( user, request=None ):
        """ user -> {}
        o User will implement IPropertiedUser.
        o Plugin may scribble on the user, if needed (but must still
          return a mapping, even if empty).
        o May assign properties based on values in the REQUEST object, if
          present
        """

Aquí hay un ejemplo simple::

    def getPropertiesForUser(self, user, request=None):
        return { "email" : user.getId() + "@ourcompany.com" }

esto agrega una propiedad *email* a un usuario que está codificada para que el id de usuario
seguido del nombre de dominio de la empresa.
