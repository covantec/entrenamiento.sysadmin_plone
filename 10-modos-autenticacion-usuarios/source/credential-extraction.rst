==========================
Extracción de credenciales
==========================

.. contents :: :local:

.. admonition:: Descripción

        Dentro de las credenciales PAS son un conjunto de información que puede identificar y autenticar un usuario.
        A los usuarios de inicio de sesión y contraseña son, por ejemplo, credenciales muy comunes. También puede utilizar
        una cookie de HTTP para rastrear a los usuarios; si lo hace la cookie será su credencial.

Los plugins de extracción de credenciales de usuario PAS encontrara todas las credenciales en una solicitud. La autenticación de estas credenciales se realiza en una etapa posterior por plugin de autenticación separado.

Escribiendo un plugin
---------------------

Si desea escribir su propio plugin de extracción credencial tiene que implementar la interfaz IExtractionPlugin. Esta interfaz solo tiene un único método::

   def extractCredentials( request ):
       """ request -> {...}
       o Return a mapping of any derived credentials.
       o Return an empty mapping to indicate that the plugin found no
         appropriate credentials.
       """

Aquí hay un ejemplo simple::

   def extractCredentials(self, request):
       login=request.get("login", None)

       if login is None:
           return {}

       password="request.get("password", None)

       return { "login" : login, "password" : password }

Este plugin extrae el nombre de usuario y contraseña en los campos con el mismo nombre en el objeto request.

