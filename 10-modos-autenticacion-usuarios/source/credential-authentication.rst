=============================
Autenticación de credenciales
=============================

.. contents :: :local:

.. admonition:: Descripción

        Las credenciales como devuelven por los plugins de extracción de credenciales sólo reflejan la
        información de autenticación proporcionada por el usuario. Estas credenciales deben ser autenticados
        por un plugin de autenticación para comprobar si son correctas para un usuario real.

La interfaz IAuthenticationPlugin es simple::

   def authenticateCredentials( credentials ):
       """ credentials -> (userid, login)
       o 'credentials' will be a mapping, as returned by IExtractionPlugin.
       o Return a  tuple consisting of user ID (which may be different
         from the login name) and login
       o If the credentials cannot be authenticated, return None.
       """

Aquí hay un ejemplo simple::

   def authenticateCredentials(self, credentials):
       users={ "hanno" : "hannosch", "martin" : "optilude",
               "philipp" : "philiKON" }

       if "login" not in credentials or "password" not in credentials:
           return None

       login=credentials["login"]
       password=credentials["password"]
       if users.get(login, None)==password:
           return (login, login)

       return None

Este plugin permite a los usuarios *hanno*, *martin* y *philipp* inicien sesión con su nickname como contraseña.
