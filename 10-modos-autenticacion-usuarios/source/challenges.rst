========
Desafíos
========

.. contents :: :local:

.. admonition:: Descripción

        Si el usuario actual (posiblemente anónimo) no está autorizado a acceder a un recurso Zope pide
        a PAS para desafiar el usuario. Generalmente esto se traducirá en un formulario de acceso que se muestra, pidiendo
        el usuario con una cuenta con privilegios adecuados.

Los plugins IChallengeProtocolChooser y IChallengePlugins trabajan juntos para hacer esto. Desde Zope se puede acceder a través de diversos protocolos (navegadores, WebDAV, XML-RPC, etc.) PAS primero tiene que averiguar qué tipo de protocolo que se está ocupando. Esto se hace mediante la consulta de todos los plugins IChallengeProtocolChooser. La implementación por defecto es ChallengeProtocolChooser, que pide a todos los plugins IRequestTypeSniffer para la prueba de protocolos específicos.

Una vez que la lista de protocolo ha sido incorporada PAS verá todos los plugins activos IChallengePlugins.
Escribir un plugin

La interfaz IChallengePlugin es muy simple: sólo contiene un método::

   def challenge( request, response ):
       """ Assert via the response that credentials will be gathered.
       Takes a REQUEST object and a RESPONSE object.
       Returns True if it fired, False otherwise.
       Two common ways to initiate a challenge:
         - Add a 'WWW-Authenticate' header to the response object.
           NOTE: add, since the HTTP spec specifically allows for
           more than one challenge in a given response.
         - Cause the response object to redirect to another URL (a
           login form page, for instance)
       """

El plugin puede mirar el objeto request para determinar qué, o si, tiene que hacer. A continuación, puede modificar el objeto response para emitir su reto para el usuario. Por ejemplo::

   def challenge(self, request, response):
       response.redirect("http://www.disney.com/")
       return True

Esto redireccionará al usuario a la página principal de Disney cada vez que intente acceder a algo que no está autorizado.
