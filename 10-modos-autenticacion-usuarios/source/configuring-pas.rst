================
Configurando PAS
================

.. contents:: :local:

.. admonition:: Descripción

    No hay una interfaz Plone para configurar el PAS:
    usted necesitara usar la Zope Management Interface (`ZMI`_).
    En la ZMI usted vera la carpeta ``acl_users`` en el raíz del sitio.
    Este es su objeto PAS.

Si usted abre la carpeta ``acl_users`` usted verá
un número de diferentes elementos.
Cada elemento es un plugin PAS, el cual implementa algunas funcionalidades PAS.

.. image:: pas-contents.jpg
   :width: 400 px
   :alt: El contenido de una carpeta user en el ZMI

Hay un elemento especial: el objeto ``plugins`` gestiona
toda la contabilidad administrativa dentro de PAS.
Se recuerda que las interfaces están activas para cada plugin
y en qué orden los plugins deben ser llamados.

Echemos un vistazo a ver cómo funciona esto.
Si abre el objeto ``plugins``
verá una lista de todas las interfaces de PAS,
junto con una breve descripción de lo que hacen.

Vamos a echar un vistazo a los plugins de extracción.
Estos plugins se encargan de extraerlo las credenciales
tal como su nombre de usuario y la contraseña de una solicitud HTTP.
Estas credenciales se pueden utilizar para autenticar al usuario.
Si hace clic en el header de los Plugins de Extracción verá
una pantalla que muestra los plugins que implementan esta interfaz
y le permite configurar los plugins que se utilizarán y en qué orden.

.. image:: extraction-interface-config.jpg
   :width: 400 px
   :alt: Configuración para los plugins de extracción.

En la configuración por defecto de Plone hay dos plugins habilitados para este
interfaz:

* El plugin ``credentials_cookie_auth`` puede extraer el nombre de usuario y
  contraseña de un valores de cookies HTTP y los valores desde formularios HTTP en el formulario de inicio de sesión o
  portlet;
* el plugin ``credentials_basic_auth`` puede extraer el nombre de usuario y
  contraseña de encabezados de autenticación HTTP estándar.

En la configuración por defecto el plugin de cookie tiene preferencia sobre la
plugin de autenticación básica.
Esto significa que las credenciales desde una cookie HTTP serán preferibles a las
credenciales de encabezados de autenticación HTTP si ambos están presentes.
Usted puede hacer esto mediante el primer logging en el uso estándar
de Autenticación HTTP en la raíz de Zope, y luego visitar su sitio Plone
e inicio de sesión con un usuario diferente allí: usted verá que el nuevo usuario
ahora es el usuario activo.

Puede cambiar el orden de los plugins haciendo clic en un plugin y moviendo
hacia arriba o hacia abajo con los botones de las flechas. Con las flechas izquierda y derecha se puede
habilitar y deshabilitar un plugin para esta interfaz.

.. _ZMI: https://plone-spanish-docs.readthedocs.io/es/latest/zope/zmi/index.html