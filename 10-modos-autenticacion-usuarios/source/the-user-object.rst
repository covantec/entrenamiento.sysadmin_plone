===============
El objeto user
===============

.. contents:: :local:

.. admonition:: Descripción

    En contraste con otras carpetas user, un usuario en un entorno de PAS
    no tiene una sola fuente.
    Los diversos aspectos de un usuario (propiedades, grupos, roles, etc.) son
    gestionado por diferentes plugins.
    Para acomodar esto, PAS presenta un objeto user que proporciona un
    interfaz única a todos los aspectos diferentes.

Hay dos tipos básicos de usuario:
un usuario normal (como se define por el interfaz ``IBasicUser``)
y un usuario con propiedades de miembro
(definida por la interfaz de ``IPropertiedUser``).
Dado que los usuarios básicos no se utilizan en Plone serán solo consideraremos
usuarios ``IPropertiedUser``.

``getId()``
   devuelve el id de usuario. Este es un id único para un usuario.

``getUserName()``
   Devuelve el nombre de inicio de sesión usado para el usuario para iniciar sesión en el sistema.

``getRoles()``
   Devuelve los roles asignados a un usuario a nivel "global".

``getRolesInContext(context)``
   Devuelve los roles asignados al usuario dentro de un contexto específico.
   Este incluye los roles globales como devuelto por ``getRoles()``.
