============
Introducción
============

.. contents:: :local:

.. admonition:: Descripción

    El Pluggable Authentication Service (PAS) es una alternativa
    a la Zope User Folder estándar
    o al popular Group User Folder (GRUF).
    PAS tiene un diseño altamente modular, el cual es
    muy poderoso, pero también es muy duro de entender.

PAS se construye alrededor de los conceptos de interfaces y plugins:
todas las tareas posibles relacionadas con la gestión de usuarios, grupos y autenticación se describen en interfaces separadas.
Esas interfaces son implementadas por plugins
los cuales puede ser selectivamente habilitados por interfaz.

Plone usa PlonePAS, el cual extiende PAS con un par de tipos de plugins adicionales
y el cual agrega la compatibilidad GRUF.
Desde extensiones PlonePAS rara vez se necesitan y están sujetos a cambio
en las nuevas versiones de Plone, este tutorial se centrará sólo en características puros PAS.