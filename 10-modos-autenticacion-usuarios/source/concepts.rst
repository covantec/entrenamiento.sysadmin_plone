=========
Conceptos
=========

.. contents:: :local:

.. admonition:: Descripción

    PAS tiene unos conceptos básicos que usted debe entender para 
    desarrollar código relacionado con el PAS.

Esos son unos conceptos básicos usados en el PAS:

**credentials**
   Las credenciales son un conjunto de información que se puede utilizar para autenticar
   a un usuario.
   Esto puede ser un nombre de usuario y contraseña, una dirección IP, una cookie de sesión
   o algo más.

**user name**
   El user name es el nombre utilizado por el usuario para iniciar sesión en el sistema.
   Para evitar la confusión entre "user id" y "user name" en este tutorial
   se utilizar el termino de nombre de inicio de sesión en su lugar.

**user id**
   Todos los usuarios deben ser identificados de forma única por su id de usuario.
   Un id de un usuario puede ser diferente que el nombre de inicio de sesión name.

**principal**
   Un principal es un identificador para cualquier entidad dentro del autenticación
   sistema.
   Esto puede ser ya sea un usuario o un grupo.
   Esto implica que ¡no es legal tener un usuario y un grupo con el
   mismo id!