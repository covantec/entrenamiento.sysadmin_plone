============
Repositorios
============

.. contents :: :local:

PAS
---

* Código base genérico de Zope https://github.com/zopefoundation/Products.PluggableAuthService

* `Código base Plone <https://github.com/plone/Products.PlonePAS/blob/master/README.rst>`_

Repositorio de Plugin para PAS
==============================

Plugins Listo o mal concebidas:

http://svn.plone.org/svn/collective/PASPlugins/

Un montón de ejemplos para la configuración dinámica de grupo.
