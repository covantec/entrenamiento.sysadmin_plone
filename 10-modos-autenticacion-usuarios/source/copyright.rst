.. -*- coding: utf-8 -*-

.. acerca_copyright:

=========
Copyright
=========

Plone y el logo Plone
=====================

Plone® y el logotipo de Plone son marcas registradas por la `Fundación Plone`_.

Plone® and the Plone Logo are registered trademarks of `The Plone Foundation`_.


Autores originales
==================

Todas esta documentación es licencia en la GNU General Public License version 2 por:

* Copyright © 2010 - 2015 `Plone Foundation`_ de la comunidad Plone.

Traducción al Español
=====================

Todas esta traducción de esta documentación es licencia bajo la Creative Commons
Reconocimiento-CompartirIgual 3.0 Venezuela por:

* Copyright © 2015 - 2021 `Covantec R.L.`_. Algunos derechos reservados.

  * Leonardo J. Caballero G. <leonardoc@plone.org>.

.. seealso:: Ver :ref:`licencias <licencias>` para información completa sobre los términos y licencia.

.. _Fundación Plone: https://plone.org/foundation/
.. _The Plone Foundation: https://plone.org/foundation/
.. _Plone Foundation: https://github.com/plone/plone.app.caching/graphs/contributors
.. _Covantec R.L.: https://github.com/Covantec
