=========================
Algoritmo de Autorización
=========================

.. contents:: :local:

Estos son los pasos de la carpeta user de PAS sigue en su método ``validate``:

#. extraer todas las credenciales.
   Este busca cualquier posible formulario de información de autenticación en una
   request: cookies HTTP, parámetros del formulario HTTP, las headers de autenticación
   HTTP, dirección IP de origen, etc.
   Una request puede tener varios (o no) conjuntos de credenciales.

#. para cada conjunto de credenciales encontradas:

   #. tratar de autorizar las credenciales.
      Esto comprueba si las credenciales corresponden a un usuario conocido y son
      válido.
   #. crear una instancia de usuario
   #. tratar de autorizar el request.
      Si tiene éxito, utilice este usuario y detener el procesamiento.

#. crear un usuario usuario anónimo

#. tratar de autorizar el request utilizando el usuario anónimo.
   Si el uso exitoso de esto, si no:

#. lanzar un desafío.
