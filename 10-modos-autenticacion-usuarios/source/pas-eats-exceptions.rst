====================
PAS come excepciones
====================

.. contents :: :local:

Una carpeta user rota es una de las peores cosas que pueden suceder en Zope: puede hacer que sea imposible acceder a cualquier objeto debajo del nivel de las carpetas de usuario.

Con el fin de garantizar su propia seguridad contra errores en plugins PAS ignora todas las excepciones de los tipos de excepción comunes: NameError, AttributeError, KeyError, TypeError y ValueError.

Esto puede hacer que la depuración de plugins: un error en un plugin puede ser ignorada en silencio si su excepción es tragado por el PAS.

No trague
---------

Usyed puede decir PAS no tragar sus excepciones estableciendo la
atributo ``_dont_swallow_my_exceptions`` en la clase de plugin.

Desde ``Products/PluggableAuthService/PluggableAuthService.py`` linea 86::

    # except if they tell us not to do so
    def reraise(plugin):
        try:
            doreraise = plugin._dont_swallow_my_exceptions
        except AttributeError:
            return
        if doreraise:
            raise

Lo que significa para tomar ventaja de esta característica, hacer algo como esto en su
clase de plugin::

    class LoginOnlyOncePlugin(BasePlugin):
        """
        Class methods via Products/PluggableAuthService/interfaces/plugins.py
        """

        meta_type = 'Login Only Once Plugin'
        security = ClassSecurityInfo()
        _dont_swallow_my_exceptions = True

        def __init__(self, id, title=None):
            self._setId(id)
            self.title = title

        ...



