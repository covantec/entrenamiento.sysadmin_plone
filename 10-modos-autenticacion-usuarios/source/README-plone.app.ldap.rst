.. -*- coding: utf-8 -*-

Panel control LDAP para Plone
=============================

Visión general
--------------

plone.app.ldap ofrece una interfaz de usuario en un sitio Plone para gestionar
LDAP y servidores de Active Directory. 

Este paquete tiene éxito gracias el paquete simplon.plone.ldap.

Se basa en la funcionalidad proporcionada por LDAPMultiPlugins_, LDAPUserFolder_
y PloneLDAP_.


Active Directory
----------------

Active Directory proporciona una interfaz LDAP para sus datos. Utilizando esta interfaz Plone
se puede usar tanto para usuarios y grupos de un sistema de Active Directory. La escritura a
Active Directory no esta soportada.

Con Active Directory puede utilizar dos propiedades diferentes como el nombre de inicio de sesión:
`userPrincipalName` y `sAMAccountName`. `sAMAccountName` es el nombre de la cuenta normal
sin ninguna información de dominio y sólo es único dentro de un solo dominio.
Si su entorno sólo utiliza un único dominio AD esta opción es la mejor
elección. Para entornos con múltiples nombres del atributo `userPrincipalName`
se puede utilizar ya que este incluye tanto nombre de cuenta y la información de dominio.

Desde Plone no soporta con los id de usuario binarios no es posible utilizar el
atributo `objectGUID` como id de usuario. En su lugar se puede usar `sAMAccountName`
o `userPrincipalName`. Los mismos criterios para elegir un nombre de usuario también
aplicará a la selección del atributo id de usuario.

LDAP Estándar
-------------

Los servidores de directorio LDAP son totalmente compatibles. Los usuarios y grupos LDAP
son utilizables como usuarios y grupos estándar de Plone se puede me gestionan normalmente.
Creación y eliminar usuarios y grupos están soportadas.


Instalando
----------

Este paquete trabaja con Plone 3 y Plone 4. Los usuarios de Plone 3 y Plone 4.0
deben instalar una versión en la serie 1.2.* 
(por ejemplo plone.app.ldap < 1.3, la última versión actual es 1.2.9), como
la publicación 1.3 sólo funcionará con Plone 4.1 o superior.

Este paquete depende del paquete ``python-ldap``. Para construirla correctamente
necesita tener algunas bibliotecas de desarrollo incluidos en su sistema. En un típico
Uso de instalación basada en Debian::

    sudo apt-get install python-dev libldap2-dev libsasl2-dev libssl-dev

Una vez instalado el paquete, estará disponible como nombrado un complemento
"LDAP support", y este complemento se pueden activar en una instancia de Plone
utilizando la sección del panel de control Complementos de Plone. Tenga cuidado, ya que esto
paquete también instala actualmente LDAPUserFolder como una dependencia, lo que hace
al complemento "LDAPUserFolder CMF Tools" disponibles. ¡No instale este complemento!
Sustituirá a la herramienta portal_membership y hacer que su sitio Plone
inutilizable.

Instalar sin buildout
~~~~~~~~~~~~~~~~~~~~~

En primer lugar es necesario instalar este paquete en la PYTHONPATH para su
Instancia Zope. Esto se puede hacer mediante la instalación en cualquiera de su sistema
paquetes de trayectoria (generalmente con ``pip`` o ``easy_install``) o en el
directorio lib/python en su instancia Zope.

Después de instalar el paquete tiene que ser registrado en su instancia Zope.
Esto se puede hacer poniendo un archivo plone.app.ldap-configure.zcml en el
directorio etc/package-includes con este contenido::

  <include package="plone.app.ldap" />

o, alternativamente, se puede añadir que la línea a la configure.zcml en un
paquete o producto que ya está registrado.

Instalando con buildout
~~~~~~~~~~~~~~~~~~~~~~~

Si está utilizando `buildout`_ para gestionar la instancia, instalar el paquete plone.app.ldap
es aún más simple. Puede instalarlo agregue una nueva línea la declarativa eggs para su
instancia zope::

  [instance]
  eggs =
      ...
      plone.app.ldap

.. _buildout: https://pypi.org/project/zc.buildout


Instalando la versión en desarrollo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para especificar la versión de desarrollo actual puede utilizar::

  [buildout]
  find-links =
      ...
      http://github.com/plone/plone.app.ldap/tarball/master#egg=plone.app.ldap-dev

  [instance]
  eggs =
      ...
     plone.app.ldap==dev

Con el comando ``pip`` eso podría ser así::

  pip install -f \ 
  http://github.com/plone/plone.app.ldap/tarball/master#egg=plone.app.ldap-dev \ 
  plone.app.ldap==dev

Con el comando ``easy_install``::

  easy_install -f \ 
  http://github.com/plone/plone.app.ldap/tarball/master#egg=plone.app.ldap-dev \ 
  plone.app.ldap==dev


Copyright y créditos
--------------------

Copyright
    plone.app.ldap is Copyright 2007, 2008 by the Plone Foundation.
    Simplon_ donated the simplon.plone.ldap code to the Plone Foundation.

Créditos
     Wichert Akkerman <wichert@simplon.biz>

Funding
     CentrePoint_


.. _simplon: https://www.linkedin.com/company/simplon-b.v./
.. _python-ldap: http://python-ldap.sourceforge.net/
.. _LDAPUserFolder: https://pypi.org/project/Products.LDAPUserFolder/
.. _LDAPMultiPlugins: https://pypi.org/project/Products.LDAPMultiPlugins/
.. _PloneLDAP: https://pypi.org/project/Products.PloneLDAP/
.. _CentrePoint: https://centrepoint.org.uk/
