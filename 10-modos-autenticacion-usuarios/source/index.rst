.. -*- coding: utf-8 -*-

===================================
 Pluggable Authentication Service
===================================

.. admonition:: Description

    El Pluggable authentication service (PAS)
    provee una forma fácil para personalizar la manera en que sus usuarios son
    autenticados y le he asignados roles.

.. toctree::
   :maxdepth: 2

   introduction
   features-and-interfaces
   the-important-interfaces
   configuring-pas
   configuring-an-individual-pas-plugin
   concepts
   the-user-object
   user-creation
   user-factory-plugin
   properties-plugins
   group-plugins
   roles-plugin
   authorisation-algorithm
   credential-extraction
   credential-authentication
   challenges
   pas-eats-exceptions
   plugins/index
   repositories

===================
Plone PAS para LDAP
===================

.. toctree::
   :maxdepth: 2

   README-plone.app.ldap

.. 
  Apéndices
  =========

  .. toctree::
      :maxdepth: 2

      glosario

..
  https://github.com/softformance/collective.ploneldapplugin
  https://github.com/dmunicio/bda.plone.ldap
  https://github.com/nutjob4life/plone-ldap-analysis
  https://github.com/collective/pas.plugins.sqlalchemy
  https://docs.plone.org/4/en/old-reference-manuals/pluggable_authentication_service/
  http://quintagroup.com/services/authentication
  http://www.netsight.co.uk/blog/plone-in-the-enterprise-two-killer-features
  http://www.netsight.co.uk/blog/pluggable-authentication-service
  http://www.wiggy.net/presentations/2006/PAS-tutorial/
 
  http://plone.293351.n2.nabble.com/Help-how-2-configure-Pas-Plugins-SQLalchemy-td7569149.html 
