======================
Plugin de User factory
======================

.. contents:: :local:

PAS soporta multiples tipos de objetos user.

Los dos tipos de usuario por defecto son: ``IBasicUser`` y ``IPropertiesUser``.
``IBasicUser`` es un tipo de usuario simple que soporta un identificador de usuario,
nombre de usuario, los roles y las restricciones de dominio.
``IPropertiesUser`` extiende este tipo y agrega propiedades de usuario.

Un plugin user factory crea una nueva instancia de usuario.
PAS añadirá las propiedades, los grupos y los roles a esta instancia como parte de su
proceso de creación del usuario.

Si no hay ningún plugin de user factory es capaz de crear un usuario, PAS caerá de nuevo a
la creación de una instancia estándar de ``PropertiedUser``.

El interfaz ``IUserFactoryPlugin`` es simple que contiene un solo
método::

    def createUser( user_id, name ):
        """ Return a user, if possible.
        o Return None to allow another plugin, or the default, to fire.
        """

El comportamiento por defecto PAS se demuestra por este código::

    def createUser(self, user_id, name):
        return ProperiedUser(user_id, name)
