.. -*- coding: utf-8 -*-

Z3C SQLAlchemy  PAS plugin
==========================

Este paquete El paquete pas.plugins.z3csqlalchemy ayuda a los integradores de escribir plugins PAS que comunican con 
base de datos SQL utilizando el paquete z3c.sqlalchemy. Mayor ventaja es, el
integrador puede concentrarse para escribir consultas SQL (utilizando SQLAlchemy)
en lugar de la infraestructura plugin PAS.

Uso
---

Este conjunto de plugins PAS utiliza sencillas utilidades que hace consultas reales a la
base de datos. Usted no necesita utilizar cualquier esquema de base de datos predefinida.

Un ejemplo de implementación de plug-in de autenticación (autenticación de base de datos SQL 
contra Simple Machines Forum)::

    from pas.plugins.z3csqlalchemy.interfaces import ISAAuthenticationPluginUtility

    class SMFAuthenticationPlugin(object):
        implements(ISAAuthenticationPluginUtility)

        def authenticateCredentials(self, credentials, saWrapper):
            login = credentials.get('login')
            password = credentials.get('password')
            if not login or not password:
                return None

            session = saWrapper.session
            SMFMembers = saWrapper.getMapper('smf_members')
            user = session.query(SMFMembers).filter_by(
                memberName=login,
                is_activated=1
            ).first()
            if user is None:
                return None
            if hashlib.sha1(
                login+password
            ).hexdigest().lower() == user.passwd.lower():
                return login, login    
            
    
El archivo `configure.zcml`::
    
        <utility factory=".smfauthplugins.SMFAuthenticationPlugin" 
             provides="pas.plugins.z3csqlalchemy.interfaces.ISAAuthenticationPluginUtility" />

Para mas detalles verifique el modulo `pas.plugins.z3csqlalchemy.interfaces <https://team.ncvo.org.uk/repo/projects/KHNP/repos/khnp2/browse/src/pas.plugins.z3csqlalchemy-0.2/pas/plugins/z3csqlalchemy/interfaces.py>`_.

Descarga plugin
---------------

Para obtener el plugin PAS ejecute el comando:

::

    svn co http://svn.plone.org/svn/collective/PASPlugins/pas.plugins.z3csqlalchemy/trunk \
    pas.plugins.z3csqlalchemy

Referencia
----------

* `pas.plugins.z3csqlalchemy <https://team.ncvo.org.uk/repo/projects/KHNP/repos/khnp2/diff/src/pas.plugins.z3csqlalchemy-0.2/README.txt>`_.
