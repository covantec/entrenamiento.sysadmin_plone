Introducción
============

Plone es ideal para integrar con aplicaciones legado existentes en su 
organización, esa capacidad le permite comunicarse con base de datos 
relaciones.

Este articulo explica como integrar la autenticación de usuario de Zope/Plone 
en una base de datos relacional con los servidores `PostgreSQL`_, `MySQL`_.

Plugin PAS para RDBMS
---------------------

Es posible que requiera compartir las mismas cuentas de usuario 
desde de bases de datos relacionales de sus aplicaciones existes 
con Plone, esto es factible usando plugins del `Pluggable Authentication Service (PAS)`_.

Referencias
-----------

* `Pluggable Authentication Service (PAS)`_.

* `Plone User Management: LDAP vs RDBMS vs ????? <https://lionfacelemonface.wordpress.com/2009/08/06/ldap-vs-rdbms/>`_.

.. _PostgreSQL: https://es.wikipedia.org/wiki/PostgreSQL
.. _MySQL: https://es.wikipedia.org/wiki/MySQL
.. _Pluggable Authentication Service (PAS): https://docs.plone.org/4/en/old-reference-manuals/pluggable_authentication_service/
