.. -*- coding: utf-8 -*-

=============================
SQL PAS plugin con PostgreSQL
=============================

Este articulo explica como integrar la autenticación de usuario de Zope/Plone 
en una base de datos relacional con el servidor PostgreSQL en Debian Wheezy.

El caso de uso
--------------

**Sitio Plone con usuarios existentes**

  Si poseen aplicaciones propias con una base de usuario existentes que requieren 
  puedan iniciar sesión de usuario, entonces hay una necesidad compartir las mismas 
  cuentas de usuario. Por suerte, el producto Pluggable Authentication Service (PAS)
  sirve esas necesidades.

  .. tip::
  
      Para este caso el paquete **pas.plugins.sqlalchemy** es ideal para eso.

El paquete pas.plugins.sqlalchemy
---------------------------------

Este paquete proporciona una implementación del plugin PAS para Zope 2 (Pluggable
Authentication Service), basado en la capa de abstracción de base de datos SQLAlchemy.

Se usa el paquete ``pas.plugins.sqlalchemy`` para almacenar y hacer consultas de 
usuarios/grupos utilizando la estructura de datos SQL que provee este plugin PAS.

Requerimientos
==============

Los requerimientos mínimos de instalación en Debian Wheezy son:

* Python 2.7, viene incorporado en el sistema.

* PostgreSQL server = 9.1, no viene incorporado en el sistema, 
  siga los pasos de instalación :ref:`aquí <install_debian_wheezy>`.

* Plone = 4.3.4, no viene incorporado en el sistema, siga los pasos
  de la sección *Instalación*.

* pas.plugins.sqlalchemy = 0.3, siga los pasos de la sección *Instalación*.

  * SQLAlchemy = 0.8.3, siga los pasos de la sección *Instalación*.

  * z3c.saconfig = 0.13, siga los pasos de la sección *Instalación*.

  * zope.sqlalchemy = 0.7.3, siga los pasos de la sección *Instalación*.

* psycopg2 = 2.4.4, siga los pasos de la sección *Instalación*.

Instalación
===========

Para lograr esta instalación y configuración usaremos el proyecto ``buildout.sqlpas``.

Dependencias
------------

Para cumplir con los requerimientos mínimos de instalación en Debian Wheezy,
ejecute el siguiente comando:

.. code-block:: sh

    # aptitude install gcc g++ make tar unzip bzip2 libssl-dev libxml2-dev \
                       zlib1g-dev libjpeg62-dev libreadline6-dev readline-common \
                       wv xpdf-utils python2.7-dev libxslt1-dev libpq-dev git-core

Configuración previa de PostgreSQL
----------------------------------

Para esto debe instalado y en funcionamiento el servidor PostgreSQL, sino es así, 
por favor, siga los pasos de instalación :ref:`aquí <install_debian_wheezy>`.

Es necesario como configuración previa del servidor PostgreSQL crear la base de 
datos y un usuario que administre esta, para esto ejecute los siguientes pasos:

Iniciar sesión con usuario **postgres**, con el siguiente comando:

.. code-block:: sh

    $ su postgres

Cree un *usuario* de base de datos, con el siguiente comando:

.. code-block:: sh

    $ createuser -D -S -R -l plonusrpas

Para asignar la contraseña debe conectarse al servidor PostgreSQL, con
el siguiente comando:

.. code-block:: sh

    $ psql postgres

Esta la sesión conectado altere el usuario asignando una contraseña
cifrada, con el siguiente comando:

.. code-block:: sh

    postgres=# ALTER USER plonusrpas WITH ENCRYPTED PASSWORD 'plonusrpas';
    postgres=# \q

Cree una *base de datos*, con el siguiente comando:

.. code-block:: sh

    $ createdb -Ttemplate0 -O plonusrpas -EUTF-8 plonepas

Otorgue todos los privilegios a la base de datos ``plonepas`` al usuario
``plonusrpas``, con los siguientes comando:

.. code-block:: sh

    $ psql postgres
    postgres=# GRANT ALL PRIVILEGES ON DATABASE plonepas TO plonusrpas;
    postgres=# \q


Dependiendo del requerimiento usted debe hacer una *Configuración de acceso local* 
o una *Configuración de acceso remoto* para el usuario ``plonusrpas`` usando la 
base de datos ``plonepas``, para mas información consulte :ref:`aquí <pgsql_configuracion_acceso>`.

.. note:: Y de esta forma **¡está listo para trabajar con la base de datos!**.

Descargar configuraciones
-------------------------

Para descargar el código fuente de las configuraciones buildout, ejecute el siguiente 
comando:

.. code-block:: sh

    $ git clone https://github.com/Covantec/buildout.sqlpas.git

Crear entorno virtual
---------------------

Para la inicialización del proyecto Buildout, ejecute el siguiente comando:

.. code-block:: sh

    $ cd buildout.sqlpas
    $ virtualenv python2.7
    $ source ./python2.7/bin/activate

Inicialización del proyecto
---------------------------

Para la inicialización del proyecto Buildout, ejecute el siguiente comando:

.. code-block:: sh

    (python2.7)$ pip install -r requeriments.txt
    (python2.7)$ python bootstrap.py

Construcción del proyecto
-------------------------

Para la construcción las librerías que integran con PostgreSQL con el PAS,
ejecute el siguiente comando:

.. code-block:: sh

    (python2.7)$ ./bin/buildout -vvvvvNc postgresql.cfg

Configuración ZCML de PAS Plugin
--------------------------------

Es necesario configurar una declarativa ZCML del PAS Plugin en Plone mediante
la adaptación de la declarativa ``zcml-additional`` con los parámetros de conexión 
a la *base de datos*, *usuario* y *host* del PostgreSQL, esto se declara en la sección 
``[instance]`` del archivo ``postgresql.cfg`` de la configuración buildout:

.. code-block:: xml

    zcml-additional =
        <configure xmlns="http://namespaces.zope.org/zope"
                   xmlns:db="http://namespaces.zope.org/db">
          <include package="z3c.saconfig" file="meta.zcml" />
          <db:engine name="pas"
                     url="postgresql://plonusrpas:plonusrpas@localhost/plonepas" />
          <db:session name="pas.plugins.sqlalchemy" engine="pas" />
        </configure>

Para que estos cambios sean aplicado debe volver a construir su proyecto ``buildout``,
ejecutando el siguiente comando:

.. code-block:: sh

    (python2.7)$ ./bin/buildout -vvvvvNc postgresql.cfg

Ejecutar Zope
--------------

Para iniciar la instancia Zope, ejecute el siguiente comando:

.. code-block:: sh

    (python2.7)$ ./bin/instance fg

.. tip::

    Para detener la instancia Zope, presione la combinación de teclas ``Crtl + C``.

Activar de PAS Plugin
---------------------

Para configurar el PAS Plugin en un *nuevo sitio Plone* debe realizar la secuencias
de pasos a continuación descritos:

#. Puede acceder a la dirección URL http://localhost:8080/

#. Crear su sitio Plone presionando el botón *Crear un nuevo sitio Plone*

#. Agregue el Identificador de ruta *(id del sitio)* con *Plone*.

#. Habilite el complemento llamado ``SQLAlchemy PAS install``

#. Presione el botón *Crear un sitio Plone*.

.. warning::

    Para esto es necesario tener creado un usuario y una base de datos en el 
    servidor PostgreSQL.

.. tip::

    Tenga en cuenta que al instalar el complemento ``SQLAlchemy PAS install``
    las tablas SQL proveídas por defecto por el paquete ``pas.plugins.sqlalchemy``
    se crearán automáticamente en la instalación del plugin PAS.

Configuraciones de acl_users
----------------------------

Una vez habilitado y configurado el complemento ``SQLAlchemy PAS install`` valla a 
la dirección URL http://localhost:8080/Plone/acl\_users/ y tendrá instalado el plugin 
``sql (SQLAlchemy user/group/prop store)`` dentro en la carpeta ``acl_users`` 
(Ver Figura 2.1).

.. figure:: ../_static/sql_plugin.png
   :align: center
   :alt: El plugin sql (SQLAlchemy user/group/prop store)

   El plugin sql (SQLAlchemy user/group/prop store)

Valla a la dirección URL http://localhost:8080/Plone/acl\_users/plugins, y haga clic en 
``User_adder Plugins`` y mueva plugin llamado ``sql`` hacia arriba como de primero 
en la lista de selección múltiple ``Active Plugin``, esto cambiara que las cuentas 
de usuarios son almacenadas en tablas SQL (Ver Figura 2.2).

.. figure:: ../_static/user_adapter_plugins.jpg
   :align: center
   :alt: El plugin sql en User_adder Plugins

   El plugin sql en User_adder Plugins

Valla a la dirección URL http://localhost:8080/Plone/acl\_users/sql para mas información 
acerca del plugin ``SQLAlchemy user/group/prop manager`` (Ver Figura 2.3).

.. figure:: ../_static/sql_plugin_details.png
   :align: center
   :alt: Detalles del SQLAlchemy user/group/prop manager

   Detalles del SQLAlchemy user/group/prop manager

Comprobar estructuras SQL
-------------------------

Pruebe el inicio de sesión a la base de datos ``plonepas`` usando el usuario 
``plonusrpas``, con el siguiente comando:

.. code-block:: sh

    $ psql -d plonepas -U plonusrpas

Consulte las tablas creadas por el paquete ``pas.plugins.sqlalchemy``,
con el siguiente comando:

.. code-block:: sh

    plonepas=# \d
                     List of relations
     Schema |        Name        |   Type   |  Owner   
    --------+--------------------+----------+----------
     public | group_members      | table    | postgres
     public | groups             | table    | postgres
     public | principals         | table    | postgres
     public | principals_id      | sequence | postgres
     public | role_assignment_id | sequence | postgres
     public | role_assignments   | table    | postgres
     public | users              | table    | postgres
    (7 rows)

Consulte el detalle de la tabla ``users`` en la base de datos ``plonepas``,
con el siguiente comando:

.. code-block:: sh

  plonepas=# \d users
                      Table "public.users"
        Column      |            Type             | Modifiers 
  ------------------+-----------------------------+-----------
   id               | integer                     | not null
   login            | character varying(64)       | 
   password         | character varying(64)       | 
   salt             | character varying(12)       | 
   enabled          | boolean                     | not null
   email            | character varying(40)       | 
   portal_skin      | character varying(20)       | 
   listed           | integer                     | 
   login_time       | timestamp without time zone | 
   last_login_time  | timestamp without time zone | 
   fullname         | character varying(40)       | 
   error_log_update | double precision            | 
   home_page        | character varying(40)       | 
   location         | character varying(40)       | 
   description      | text                        | 
   language         | character varying(20)       | 
   ext_editor       | integer                     | 
   wysiwyg_editor   | character varying(10)       | 
   visible_ids      | integer                     | 
  Indexes:
      "users_pkey" PRIMARY KEY, btree (id)
      "ix_users_login" UNIQUE, btree (login)
      "ix_users_email" btree (email)
      "ix_users_enabled" btree (enabled)
      "ix_users_fullname" btree (fullname)
  Foreign-key constraints:
      "users_id_fkey" FOREIGN KEY (id) REFERENCES principals(id)

Probar integración de BD y Plone
--------------------------------

Hasta este punto todo debe funcionar la integración de la BD relacional y Plone, 
así que es tiempo de probar nuestras configuraciones: 

Para esto debe:

#. Crear un usuario desde el panel de control **Usuarios y Grupos** de Plone. (Ver Figura 2.4).

.. figure:: ../_static/usergroup_userprefs.png
   :align: center
   :alt: Usuario creado en panel de control Usuarios y Grupos

   Usuario creado en panel de control *Usuarios y Grupos*

Compruebe la creación del usuario de la base de datos ``plonepas``, consultando 
en la tabla ``users`` para esto ejecute el siguiente comando SQL:

.. code-block:: sh

    plonepas=> SELECT login, email, fullname FROM users ;
     login  |      email      |  fullname  
    --------+-----------------+------------
     jperez | jperez@mail.com | José Pérez
    (1 row)

Ahora al momento de cambiar, uno de los plugins **source\_users** o **sql** se
pueden elegir para proporcionar un servicio de autenticación. Por ejemplo, cuando 
se utiliza el plugin **sql**, después que el usuario cambie la contraseña del 
usuario a través de la interfaz de usuario de Plone, usted verá en la tabla de SQL 
las columnas correspondiente, la contraseña y sus valores actualizados.

Referencias
-----------

* `Plone SQLAlchemy PAS Plugin <http://marrtw.blogspot.com/2012/05/plone-sqlalchemy-pas.html>`_.

* `How To Install and Use Zope 2 and PostgreSQL on Ubuntu 13.10 <https://www.digitalocean.com/community/tutorials/how-to-install-and-use-zope-2-and-postgresql-on-ubuntu-13-10>`_.

* `ComponentLookupError on pas.plugins.sqlalchemy <http://plone.293351.n2.nabble.com/ComponentLookupError-on-pas-plugins-sqlalchemy-td5375932.html>`_.
