.. -*- coding: utf-8 -*-

.. _install_mysql_debianwheezy:

=================================================
Instalación de un servidor MySQL en Debian Wheezy
=================================================

Este articulo explica como instalar el servidor y un cliente de lineas
de comandos de la base de datos MySQL en Debian Wheezy.

.. note::
    Fue publicado inicialmente en https://lcaballero.wordpress.com/2010/08/16/instalacion-de-un-servidor-mysql-en-debian-lenny/

Instalación
-----------

Para instalar el servidor y un cliente de lineas de comandos MySQL,
ejecute el siguiente comando:

.. code-block:: sh

    # aptitude install mysql-server mysql-client

Tenga en cuenta que hemos instalado las librerías y los encabezados de
desarrollo con el paquete ***‘libmysqlclient15-dev’*** las cuales se
puede dejar por fuera, pero he encontrado que son útiles en muchas
situaciones.

Contraseña MySQL
----------------

Durante la instalación de MySQL, se le presentará la opción de
establecer una contraseña:


.. figure:: ../_static/Mysql_password.png
   :align: center
   :alt: Escribir contraseña de root MySQL

   Escribir contraseña de root MySQL

Estableciendo la contraseña de usuario administrador “root” de MySQL es
un paso recomendado es su instalación. Si usted decide que la protección
de su base de datos de producción es una buena idea, entonces
simplemente escriba su contraseña elegida como se indica previamente.

A diferencia de versiones anteriores de Debian, en la versión de Debian
Wheezy pide una confirmación de la contraseña (¡lo cual es una buena
cosa!):

.. figure:: ../_static/Mysql_password_confirm.png
   :align: center
   :alt: Confirmar contraseña de root MySQL, introducida previamente

   Confirmar contraseña de root MySQL, introducida previamente

Creando base de datos
---------------------

Primero tiene que iniciar sesión como usuario “root” de MySQL, con el
siguiente comando:

.. code-block:: sh

    $ mysql -u root -p
    Enter password:

Entonces coloque la contraseña del usuario “root” definida previamente.

Luego de iniciar sesión en el servidor como “root”, ahora usted puede
crear una BD, con el siguiente comando:

.. code-block:: sql

    mysql> CREATE DATABASE mibasededatos;

Donde **mibasededatos** es el nombre real de su base de datos y por
tanto debe ser sustituido por el nombre real. Para comprobar que la base
datos se creo ejecute el siguiente comando:

.. code-block:: sql

    mysql> SHOW DATABASES;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | mibasededatos      |
    | mysql              |
    +--------------------+
    3 rows in set (0.00 sec)

A continuación salga de la sesión de MySQL

.. code-block:: sql

    mysql> EXIT

A continuación se creará una base de datos basado en un script con todas
las sintaxis SQL para construirla. Este script se llama
“genera-base.sql”

Entonces debe ubicarse en el directorio donde esta este archivo .sql y
ejecutamos el siguiente comando para correr el script:

.. code-block:: sh

    $ mysql -u root -p mibasededatos < mibasededatos-generada.sql
    Enter password:

Para comprobar que la estructura de la base datos se creo con éxito,
ejecute los siguientes comandos:

.. code-block:: sh

    $ mysql -u root -p mibasededatos
    Enter password:

    mysql> SHOW TABLES;
    +--------------------------------+
    | Tables_in_mibasededatos        |
    +--------------------------------+
    | mi_tabla1                      |
    | mi_tabla2                      |
    | mi_tabla3                      |
    | mi_tabla4                      |
    +--------------------------------+
    4 rows in set (0.00 sec)

Creando usuarios
----------------

Vuelve a entrar como **root** para crear usuarios para esta base, en
este caso usuario **admin** con su contraseña **adminbd** con el
siguiente comando:

.. code-block:: sh

    $ mysql -u root -p
    Enter password:

Una vez conectado el servidor ejecute el siguiente comando para crear el
usuario

.. code-block:: sql

    mysql> GRANT ALL PRIVILEGES ON mibasededatos.* TO 'admin'@'localhost' \ 
    IDENTIFIED BY 'adminbd';
    Query OK, 0 rows affected (0.03 sec)

    mysql> FLUSH PRIVILEGES;
    Query OK, 0 rows affected (0.01 sec)

Para comprobar que el usuario se creo con éxito, ejecute los siguientes
comandos:

.. code-block:: sql

    mysql> SELECT user FROM mysql.user;
    +------------------+
    | user             |
    +------------------+
    | root             |
    | root             |
    | admin            |
    | debian-sys-maint |
    | root             |
    +------------------+
    5 rows in set (0.00 sec)

Luego salga para ingresar con el usuario creado:

.. code-block:: sql

    mysql> EXIT
    Bye

Ahora debe conectarse como usuario **admin** indicándole de la base de
datos **mibasededatos** de la siguiente forma:

.. code-block:: sh

    $ mysql -u admin -p mibasededatos
    Enter password:
    Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 47
    Server version: 5.0.51a-24+lenny4 (Debian)

    Type 'help;' or '\h' for help. Type '\c' to clear the buffer.

    mysql>

y de esta forma ¡esta listo para trabajar con la base de datos!

Referencias
-----------

-  `Instalación de un servidor MySQL en Debian Lenny <https://lcaballero.wordpress.com/2010/08/16/instalacion-de-un-servidor-mysql-en-debian-lenny/>`_.

-  `MySQL 5.0 Reference Manual <http://dev.mysql.com/doc/refman/5.0/es/index.html>`_.

-  `Simple MySQL cookbook <http://web.archive.org/web/*/https://debian-administration.org/article/Simple_MySQL_cookbook/print>`_.

-  `Adding new users to MySQL Databases <http://web.archive.org/web/*/https://debian-administration.org/article/Adding_new_users_to_MySQL_Databases>`_.
