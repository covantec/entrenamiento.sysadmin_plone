.. -*- coding: utf-8 -*-

========================
SQL PAS plugin con MySQL
========================

Este articulo explica como integrar la autenticación de usuario de Zope/Plone 
en una base de datos relacional con el servidor MySQL en Debian Wheezy.

El caso de uso
--------------

**Sitio Plone con usuarios existentes**

  Si poseen aplicaciones propias con una base de usuario existentes que requieren 
  puedan iniciar sesión de usuario, entonces hay una necesidad compartir las mismas 
  cuentas de usuario. Por suerte, el producto Pluggable Authentication Service (PAS)
  sirve esas necesidades.

  .. tip::
  
      Para este caso el paquete **pas.plugins.sqlalchemy** es ideal para eso.

El paquete pas.plugins.sqlalchemy
---------------------------------

Este paquete proporciona una implementación del plugin PAS para Zope 2 (Pluggable
Authentication Service), basado en la capa de abstracción de base de datos SQLAlchemy.

Se usa el paquete ``pas.plugins.sqlalchemy`` para almacenar y hacer consultas de 
usuarios/grupos utilizando la estructura de datos SQL que provee este plugin PAS.

Requerimientos
==============

Los requerimientos mínimos de instalación en Debian Wheezy son:

* Python 2.7, viene incorporado en el sistema.

* MySQL server = 5.5, no viene incorporado en el sistema, 
  siga los pasos de instalación :ref:`aquí <install_mysql_debianwheezy>`.

* Plone = 4.3.4, no viene incorporado en el sistema, siga los pasos
  de la sección *Instalación*.

* pas.plugins.sqlalchemy = 0.3, siga los pasos de la sección *Instalación*.

  * SQLAlchemy = 0.8.3, siga los pasos de la sección *Instalación*.

  * z3c.saconfig = 0.13, siga los pasos de la sección *Instalación*.

  * zope.sqlalchemy = 0.7.3, siga los pasos de la sección *Instalación*.

* Products.ZMySQLDA = 3.1.1, siga los pasos de la sección *Instalación*.

* MySQL-python = 1.2.5, siga los pasos de la sección *Instalación*.

Instalación
===========

Para lograr esta instalación y configuración usaremos el proyecto ``buildout.sqlpas``.

Dependencias
------------

Para cumplir con los requerimientos mínimos de instalación en Debian Wheezy,
ejecute el siguiente comando:

.. code-block:: sh

    # aptitude install gcc g++ make tar unzip bzip2 libssl-dev libxml2-dev \
                       zlib1g-dev libjpeg62-dev libreadline6-dev readline-common \
                       wv xpdf-utils python2.7-dev libxslt1-dev libpq-dev git-core

Configuración previa de MySQL
-----------------------------

Para esto debe instalado y en funcionamiento el servidor MySQL, sino es así, 
por favor, siga los pasos de instalación :ref:`aquí <install_mysql_debianwheezy>`.

Es necesario como configuración previa del servidor MySQL crear la base de 
datos y un usuario que administre esta, para esto ejecute los siguientes pasos:

Iniciar sesión con usuario **root** de MySQL, con el siguiente comando:

.. code-block:: sh

    $ mysql -u root -p

Entonces coloque la contraseña del usuario **root** definida previamente.

Luego de iniciar sesión en el servidor como **root**, ahora usted puede
crear una BD, con el siguiente comando:

.. code-block:: sh

    mysql> CREATE DATABASE plonepas;

Para comprobar que la base datos se creo ejecute el siguiente comando:

.. code-block:: sh

    mysql> SHOW DATABASES;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | plonepas           |
    | mysql              |
    +--------------------+
    3 rows in set (0.00 sec)


Cree un *usuario* de base de datos, con el siguiente comando:

.. code-block:: sh

    mysql> GRANT ALL PRIVILEGES ON plonepas.* TO 'plonusrpas'@'localhost' \ 
    IDENTIFIED BY 'plonusrpas';
    Query OK, 0 rows affected (0.03 sec)

    mysql> FLUSH PRIVILEGES;
    Query OK, 0 rows affected (0.01 sec)

Para comprobar que el usuario se creo con éxito, ejecute los siguientes
comandos:

.. code-block:: sh

    mysql> SELECT user FROM mysql.user;
    +------------------+
    | user             |
    +------------------+
    | root             |
    | plonusrpas       |
    | debian-sys-maint |
    +------------------+
    3 rows in set (0.00 sec)

Luego salga para ingresar con el usuario creado:

.. code-block:: sh

    mysql> EXIT
    Bye

Ahora debe conectarse como usuario **plonusrpas** indicándole de la base de
datos **plonepas** de la siguiente forma:

.. code-block:: sh

    $ mysql -u plonusrpas -p plonepas
    Enter password:
    Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 177
    Server version: 5.5.41-0+wheezy1 (Debian)

    Copyright (c) 2000, 2014, Oracle and/or its affiliates. All rights reserved.

    Oracle is a registered trademark of Oracle Corporation and/or its
    affiliates. Other names may be trademarks of their respective
    owners.

    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

Y de esta forma **¡está listo para trabajar con la base de datos!**.

Descargar configuraciones
-------------------------

Para descargar el código fuente de las configuraciones buildout, ejecute el siguiente 
comando:

.. code-block:: sh

    $ git clone https://github.com/Covantec/buildout.sqlpas.git

Crear entorno virtual
---------------------

Para la inicialización del proyecto Buildout, ejecute el siguiente comando:

.. code-block:: sh

    $ cd buildout.sqlpas
    $ virtualenv python2.7
    $ source ./python2.7/bin/activate

Inicialización del proyecto
---------------------------

Para la inicialización del proyecto Buildout, ejecute el siguiente comando:

.. code-block:: sh

    (python2.7)$ pip install -r requeriments.txt
    (python2.7)$ python bootstrap.py

Construcción del proyecto
-------------------------

Para la construcción las librerías que integran con MySQL con el PAS,
ejecute el siguiente comando:

.. code-block:: sh

    (python2.7)$ ./bin/buildout -vvvvvNc mysql.cfg

Configuración ZCML de PAS Plugin
--------------------------------

Es necesario configurar una declarativa ZCML del PAS Plugin en Plone mediante
la adaptación de la declarativa ``zcml-additional`` con los parámetros de conexión 
a la *base de datos*, *usuario* y *host* del MySQL, esto se declara en la sección 
``[instance]`` del archivo ``mysql.cfg`` de la configuración buildout:

.. code-block:: xml

    zcml-additional =
        <configure xmlns="http://namespaces.zope.org/zope"
                   xmlns:db="http://namespaces.zope.org/db">
          <include package="z3c.saconfig" file="meta.zcml" />
          <db:engine xmlns="http://namespaces.zope.org/db" name="pas" 
                     url="mysql://plonusrpas:plonusrpas@host/plonepas" />
          <db:session xmlns="http://namespaces.zope.org/db" 
                      name="pas.plugins.sqlalchemy" engine="pas" />
        </configure>

Para que estos cambios sean aplicado debe volver a construir su proyecto ``buildout``,
ejecutando el siguiente comando:

.. code-block:: sh

    (python2.7)$ ./bin/buildout -vvvvvNc mysql.cfg

Ejecutar Zope
--------------

Para iniciar la instancia Zope, ejecute el siguiente comando:

.. code-block:: sh

    (python2.7)$ ./bin/instance fg

.. tip::

    Para detener la instancia Zope, presione la combinación de teclas ``Crtl + C``.

Activar de PAS Plugin
---------------------

Para configurar el PAS Plugin en un *nuevo sitio Plone* debe realizar la secuencias
de pasos a continuación descritos:

#. Puede acceder a la dirección URL http://localhost:8080/

#. Crear su sitio Plone presionando el botón *Crear un nuevo sitio Plone*

#. Agregue el Identificador de ruta *(id del sitio)* con *Plone*.

#. Habilite el complemento llamado ``SQLAlchemy PAS install``

#. Presione el botón *Crear un sitio Plone*.

.. warning::

    Para esto es necesario tener creado un usuario y una base de datos en el 
    servidor MySQL.

.. tip::

    Tenga en cuenta que al instalar el complemento ``SQLAlchemy PAS install``
    las tablas SQL proveídas por defecto por el paquete ``pas.plugins.sqlalchemy``
    se crearán automáticamente en la instalación del plugin PAS.

Configuraciones de acl_users
----------------------------

Una vez habilitado y configurado el complemento ``SQLAlchemy PAS install`` valla a 
la dirección URL http://localhost:8080/Plone/acl\_users/ y tendrá instalado el plugin 
``sql (SQLAlchemy user/group/prop store)`` dentro en la carpeta ``acl_users`` 
(Ver Figura 2.1).

.. figure:: ../_static/sql_plugin.png
   :align: center
   :alt: El plugin sql (SQLAlchemy user/group/prop store)

   El plugin sql (SQLAlchemy user/group/prop store)

Valla a la dirección URL http://localhost:8080/Plone/acl\_users/plugins, y haga clic en 
``User_adder Plugins`` y mueva plugin llamado ``sql`` hacia arriba como de primero 
en la lista de selección múltiple ``Active Plugin``, esto cambiara que las cuentas 
de usuarios son almacenadas en tablas SQL (Ver Figura 2.2).

.. figure:: ../_static/user_adapter_plugins.jpg
   :align: center
   :alt: El plugin sql en User_adder Plugins

   El plugin sql en User_adder Plugins

Valla a la dirección URL http://localhost:8080/Plone/acl\_users/sql para mas información 
acerca del plugin ``SQLAlchemy user/group/prop manager`` (Ver Figura 2.3).

.. figure:: ../_static/sql_plugin_details.png
   :align: center
   :alt: Detalles del SQLAlchemy user/group/prop manager

   Detalles del SQLAlchemy user/group/prop manager

Comprobar estructuras SQL
-------------------------

Pruebe el inicio de sesión a la base de datos ``plonepas`` usando el usuario 
``plonusrpas``, con el siguiente comando:

.. code-block:: sh

    $ mysql -u plonusrpas -p plonepas

Consulte las tablas creadas por el paquete ``pas.plugins.sqlalchemy``,
con el siguiente comando:

.. code-block:: sh

  mysql> SHOW TABLES;
  +--------------------+
  | Tables_in_plonepas |
  +--------------------+
  | group_members      |
  | groups             |
  | principals         |
  | role_assignments   |
  | users              |
  +--------------------+
  5 rows in set (0.00 sec)

Consulte el detalle de la tabla ``users`` en la base de datos ``plonepas``,
con el siguiente comando:

.. code-block:: sh

  mysql> DESCRIBE users;
  +------------------+-------------+------+-----+---------+-------+
  | Field            | Type        | Null | Key | Default | Extra |
  +------------------+-------------+------+-----+---------+-------+
  | id               | int(11)     | NO   | PRI | NULL    |       |
  | login            | varchar(64) | YES  | UNI | NULL    |       |
  | password         | varchar(64) | YES  |     | NULL    |       |
  | salt             | varchar(12) | YES  |     | NULL    |       |
  | enabled          | tinyint(1)  | NO   | MUL | NULL    |       |  
  | email            | varchar(80) | YES  | MUL | NULL    |       |
  | portal_skin      | varchar(20) | YES  |     | NULL    |       |
  | listed           | int(11)     | YES  |     | NULL    |       |
  | login_time       | datetime    | YES  |     | NULL    |       |
  | last_login_time  | datetime    | YES  |     | NULL    |       |
  | fullname         | varchar(80) | YES  | MUL | NULL    |       |
  | error_log_update | float       | YES  |     | NULL    |       |
  | home_page        | varchar(40) | YES  |     | NULL    |       |
  | location         | varchar(40) | YES  |     | NULL    |       |
  | description      | text        | YES  |     | NULL    |       |
  | language         | varchar(20) | YES  |     | NULL    |       |
  | ext_editor       | int(11)     | YES  |     | NULL    |       |
  | wysiwyg_editor   | varchar(10) | YES  |     | NULL    |       |
  | visible_ids      | int(11)     | YES  |     | NULL    |       |
  +------------------+-------------+------+-----+---------+-------+
  19 rows in set (0.01 sec)

Probar integración de BD y Plone
--------------------------------

Hasta este punto todo debe funcionar la integración de la BD relacional y Plone, 
así que es tiempo de probar nuestras configuraciones: 

Para esto debe:

#. Crear un usuario desde el panel de control **Usuarios y Grupos** de Plone. (Ver Figura 2.4).

.. figure:: ../_static/usergroup_userprefs.png
   :align: center
   :alt: Usuario creado en panel de control Usuarios y Grupos

   Usuario creado en panel de control *Usuarios y Grupos*

Compruebe la creación del usuario de la base de datos ``plonepas``, consultando 
en la tabla ``users`` para esto ejecute el siguiente comando SQL:

.. code-block:: sh

    mysql> SELECT login, email, fullname FROM users ;
    +--------+-----------------+-------------+
    | login  | email           | fullname    |
    +--------+-----------------+-------------+
    | jperez | jperez@mail.com | Jóse Pérez  |
    +--------+-----------------+-------------+
    1 row in set (0.00 sec)

Ahora al momento de cambiar, uno de los plugins **source\_users** o **sql** se
pueden elegir para proporcionar un servicio de autenticación. Por ejemplo, cuando 
se utiliza el plugin **sql**, después que el usuario cambie la contraseña del 
usuario a través de la interfaz de usuario de Plone, usted verá en la tabla de SQL 
las columnas correspondiente, la contraseña y sus valores actualizados.

Referencias
-----------

* `[Help]how 2 configure Pas.Plugins.SQLalchemy? <http://plone.293351.n2.nabble.com/Help-how-2-configure-Pas-Plugins-SQLalchemy-td7569149.html>`_.

* TODO.

* TODO.
