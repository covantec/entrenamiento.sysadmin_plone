.. -*- coding: utf-8 -*-

SQLAlchemy PAS plugin
=====================

Este paquete proporciona una implementación del plugin PAS para Zope 2 (Pluggable
Authentication Service), basado en la capa de abstracción de base de datos SQLAlchemy.

Le permite almacenar y hacer consultas de usuarios / grupos utilizando una
base de datos SQL.

Este paquete reemplaza el producto `SQLPASPlugin
<https://pypi.org/project/pas.plugins.sqlalchemy>`_ y es técnicamente
un *fork* de ese código base. Algunas pruebas han sido reescritos pero la mayoría son
preservado.

Aunque no es proporcionado actualmente en una versión estable, se utiliza en
producción (solamente probado contra las bases de datos `pysqlite` y PostgreSQL).

Instalación
-----------

Para configurar el plugin con una base de datos, utilice el paquete
``z3c.saconfig`` y defina un named scoped session "pas.plugins.sqlalchemy" en su
archivo configure.zcml o en el parámetro "zcml-additional" de la
receta plone.recipe.zope2instance en su buildout.

Ejemplo::

  <configure xmlns="http://namespaces.zope.org/db">
    <include package="z3c.saconfig" file="meta.zcml"/>

    <engine name="pas" url="postgresql://localhost/pas" />
    <session name="pas.plugins.sqlalchemy" engine="pas" />

  </configure>

Instale el plugin el un perfil GenericSetup incluido en el paquete 
``pas.plugins.sqlalchemy``. Tenga en cuenta que las tablas se crearán automáticamente 
en la instalación.

Puede volver a instalar en cualquier momento para crear tablas no existentes.
Tenga en cuenta que tablas se conservan tras la desinstalación del paquete 
``pas.plugins.sqlalchemy``.

Configuración desde Plone
-------------------------

Como una alternativa a la especificación de la información de conexión de base de datos en ZCML, 
usted puede usar `collective.saconnect
<https://pypi.org/project/collective.saconnect>`_ para hacer sus
conexiones configurables en el panel de control de Plone.

Instale el paquete añadiéndolo a su buildout, a continuación, instalar el
complemento en su sitio Plone a través del panel de control de Plone. Usted ahora
tener un nuevo panel de control que le permite crear y editar la base de datos
conexiones.

Para agregar conexiones con la configuración genérica añadir un archivo "saconnections.xml"
al perfil de generic setup de su paquete de instalación del sitio, con la
siguiente contenido: ::

  <?xml version="1.0"?>
  <connections>
       <connection
            name="pas.plugins.sqlalchemy"
            string="postgresql://USER:PASSWORD@localhost/DATABASE"
       />
  </connections>

Mas información esta disponible en la descripción del paquete.


Modelo del el usuario, el grupo y principal personalizado
---------------------------------------------------------

Usted puede registrar su propia clase de modelo basado en SQLAlchemy para los tres
categorías.

Las interfaces de clase requeridas (métodos y atributos necesarios) son
se describe en el módulo ``interfaces``. Tenga en cuenta que usted puede simplemente
hacer una subclase de los modelos predeterminados que implementan la
interfaces que requieren.

Los configuraciones son accesibles en el ZMI. También puede utilizar una manejador 
de configuración personalizado.

Ejemplo::

    def setup_pas_plugin(self):
        pas = self.acl_users
        plugin = pas['sql']

        plugin.manage_changeProperties(
           user_model="my_package.model.User",
           principal_model="my_package.model.Principal",
           group_model="my_package.model.Group"
           )

Puede que tenga que asegurarse que los plugins este priorizado más arriba que la
los por defectos (normalmente basados en ZODB).


Wishlist
--------

These items are on the to-do list:

- Post-only security.

- Review of implemented interfaces - is the implementation complete?

- Handle groups title, description and email, to match newer versions
  of Plone.

- Tests for configuration of external model.


Créditos
--------

Autores

  - Rocky Burt <rocky@serverzen.com> of ServerZen Software

  - Nate Aune <natea@jazkarta.com> of Jazkarta

  - Stefan Eletzhofer <stefan.eletzhofer@inquant.de> of InQuant

  - Malthe Borch <mborch@gmail.com>

Contribuidores

  - Ruda Porto Filgueiras <rudazz@gmail.com>

  - Daniel Nouri <daniel.nouri@gmail.com>

  - Dorneles Treméa <deo@jarn.com> of Jarn

  - Wichert Akkerman <wichert@wiggy.net> of Simplon

  - Riccardo Lemmi <riccardo@reflab.it> of Reflab Srl

  - Derek Broughton <auspex@pointerstop.ca>

  - Rigel Di Scala <zedr>

  - Sune Broendum Woeller <woeller@headnet.dk> of Headnet Aps

Patrocinantes

  - Gracias a ChemIndustry.com Inc. por el financiamiento del desarrollo de
    SQLPASPlugin

  - Gracias a Statens Byggeforskninginstitut (https://sbi.dk/) por patrocinio 
    del soporte de caché.

  - Gracias a Gis & Web S.r.l. (http://www.gisweb.it) por patrocinio
    del soporte de administración de grupos.

  - Gracias a the Ocean Tracking Network
    (https://oceantrackingnetwork.org/) for adding Group Capabilities
    y migración de los usuarios existentes.

Licencia
--------

  GNU GPL v2 (ver archivo LICENCE.txt para mas detalles)
