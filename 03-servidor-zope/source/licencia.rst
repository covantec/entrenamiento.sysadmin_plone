.. -*- coding: utf-8 -*-

.. _licencias:

===============
Licenciamientos
===============

.. _licencia_cc:

Creative Commons Atribución-NoComercial-CompartirIgual 3.0 Unported
===================================================================

.. sidebar:: Sobre esta licencia

    Esta documentación se distribuye bajo los términos de la licencia 
    `Creative Commons Atribución-NoComercial-CompartirIgual 3.0 Unported (CC BY-NC-SA 3.0)`_.

Usted es libre de:

  * Compartir - copiar, distribuir, ejecutar y comunicar públicamente la obra.

  * Hacer obras derivadas.

Bajo los siguientes términos:

  * Atribución - Usted debe reconocer el crédito de una obra de manera adecuada, proporcionar 
    un enlace a la licencia, e indicar si se han realizado cambios . Puede hacerlo en cualquier 
    forma razonable, pero no de forma tal que sugiera que tiene el apoyo del licenciante o lo 
    recibe por el uso que hace.

  * NoComercial - Usted no puede hacer uso del material con fines comerciales.

  * CompartirIgual - Si usted mezcla, transforma o crea nuevo material a partir de esta obra, 
    usted podrá distribuir su contribución siempre que utilice la misma licencia que la obra 
    original.

Términos de la licencia: https://creativecommons.org/licenses/by-sa/3.0/deed.es

.. _Creative Commons Atribución-NoComercial-CompartirIgual 3.0 Unported (CC BY-NC-SA 3.0): https://creativecommons.org/licenses/by-nc-sa/3.0/deed.es
