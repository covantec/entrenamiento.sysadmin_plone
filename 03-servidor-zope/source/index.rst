.. -*- coding: utf-8 -*-

.. Z Object Publishing Environment documentation master file, created by
   sphinx-quickstart on Sun Aug 24 21:56:27 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

El servidor Zope
=================

**Sobre este documento**

Es documento es un compendio de varios artículos publicados previamente que hablan sobre el
servidor de aplicaciones Zope y posibles configuraciones de despliegue de aplicación.

Esta compendio de artículos toca generalidades del servidor Zope como su instalación y configuración
para aplicaciones basadas en Plone CMS.

Plone esta basado en el :ref:`servidor de aplicaciones Zope <servidor_aplicaciones_web_oao>` 
y este requiere realizar tareas de hospedaje y administrativa para un servidor de aplicación 
Zope / sitio de Plone.

.. toctree::
    :maxdepth: 2
 
    z_object_publishing_environment
    instalacion
    zmi/index
    interaccion_linea_comando
    configuraciones_generales
    configurar_como_demonio
    instancia_debug
    web/zope_plone_detras_servidor_web
    web/servidor_apache
    web/servidor_nginx
    ftp/index
    webdav/index
    wsgi/index
