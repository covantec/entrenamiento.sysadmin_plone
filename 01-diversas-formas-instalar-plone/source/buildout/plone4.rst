.. -*- coding: utf-8 -*-

.. _buildout_plone4:

=================================
Instalando Plone 4 con Buildout
=================================

.. sidebar:: Sobre este artículo

    :Autor(es): Leonardo J. Caballero G.
    :Correo(s): leonardoc@plone.org
    :Compatible con: Plone 4.x
    :Fecha: 11 de Enero de 2021

Descripción general
===================

Una herramienta para administrar, a través de un archivo de configuración
declaratoria, las partes y componentes de un desarrollo con Python. Dichas
partes no están limitadas a componentes o código Python.

La parte más poderosa de buildout es que puede extenderse con el uso de
":ref:`recetas <que_es_recipes>`" que pueden instalar componentes más
complicados simplemente agregando nuevas secciones a la configuración.
Buildout puede instalar diversos paquetes de Python fácilmente porque
está conectado con el índice de paquetes de Python (:term:`PyPI`).

Algunos términos importantes
----------------------------

Hay que entender varios conceptos antes de continuar tales como: 

- :term:`Paquete Python`.

- :term:`paquetes Egg`.

- :term:`Cheese shop`.

- :term:`Producto Zope`.

- :term:`Instalación de Zope`.

- :term:`Instancia de Zope`.

- `pip`_.

.. _buildout_plone4_requisitos:

Requisitos previos
==================

Existen instrucciones detalladas para la instalación de requisitos, pero en
general se necesita lo siguiente:

* Interprete Python 2.7.x.

* Instalar un :ref:`entornos virtuales de Python <buildoutplone4_entornovirtual>` 
  para aislar a su instalación de la instalación por defecto de su Python del sistema.

* El Python `paquete Setuptools`_ y (`pip`_).

Instalando Setuptools
---------------------

Para esto consulte los diversos `modos de instalación de setuptools y easy_install`_.

Dependencias en Debian GNU/Linux
--------------------------------

Para cumplir con los requerimientos mínimos de instalación en Debian Wheezy, ejecute el siguiente comando:

.. code-block:: sh

  # aptitude install gcc g++ make tar unzip bzip2 libssl-dev libxml2-dev \
                   zlib1g-dev libjpeg62-dev libreadline6-dev readline-common \
                   wv xpdf-utils python2.7-dev libxslt1-dev git-core

Descargar código fuente
=======================

Para descargar el código fuente, ejecute el siguiente comando:

.. code-block:: sh

  $ git clone https://github.com/Covantec/buildout.plone4.git

.. _buildoutplone4_entornovirtual:

Crear entorno virtual
=====================

Para la inicialización del proyecto Buildout, ejecute el siguiente comando:

.. code-block:: sh

  $ cd buildout.plone4
  $ virtualenv python2.7
  $ source ./python2.7/bin/activate

.. seealso::

    Consulte la referencia de `entornos virtuales de Python`_.

Inicialización del proyecto
===========================

Para la inicialización del proyecto Buildout, ejecute el siguiente comando:

.. code-block:: sh

  (python2.7)$ pip install -r requeriments.txt
  (python2.7)$ python bootstrap.py

Construcción del proyecto
=========================

Para la construcción del proyecto Buildout, ejecute el siguiente comando:

.. code-block:: sh

  (python2.7)$ ./bin/buildout

Ejecutar Zope
=============

Para iniciar la instancia Zope, ejecute el siguiente comando:

.. code-block:: sh

  (python2.7)$ ./bin/instance start

Puede acceder a la ZMI para crear su sitio Plone en la siguiente dirección http://localhost:8080/manage

Para detener la instancia Zope, ejecute el siguiente comando:

.. code-block:: sh

  ./bin/instance stop

Directorios creados
===================

.. glossary::
    :sorted:

    bin/
        Ejecutables de buildout y producidos por las partes.

    bin/buildout
        Script de zc.buildout.

    bin/instance
        Script de arranque de la instancia Zope.

    bin/zopepy
        Script para hacer inmersiones interactivas de Python en 
        el contexto de la instalación Zope / Plone.

    eggs/
        Los paquetes eggs obtenidos e instalados de :term:`PyPI`.

    downloads/
        Software adicional descargado.

    src/
        Código fuente de los paquetes Egg en desarrollos.

    parts/
        Todo el código, configuración y datos manejados por buildout.

    var/
        Los archivos Logs y archivo de la **ZODB** de Zope (buildout nunca sobre 
        escribe estos archivos).

    var/filestorage
        Contiene archivos de `ZODB`_ de Zope tales como :file:`Data.fs`, 
        :file:`Data.fs.index`, :file:`Data.fs.lock` y :file:`Data.fs.tmp` 
        de su sitio web Plone.

    var/log
        Contiene archivos de Logs de Zope tales como el archivo de errores 
        :file:`instance.log` y el archivo de acceso :file:`instance-Z2.log`.

Descripción de este ejemplo
---------------------------

Un ejemplo de una configuración buildout funcional para la ultima versión de 
Plone 4.3.x se muestra a continuación:

.. code-block:: cfg

    # definición de las partes que va a tener el buildout, cada parte es una
    # sección de configuración y generalmente utiliza una receta específica
    [buildout]
    extensions = buildout.bootstrap

    # enlaces adicionales a configuraciones adicionales buildout
    extends = 
        http://dist.plone.org/release/4.3-latest/versions.cfg
        
    # enlaces adicionales a PyPI donde pueden encontrarse paquetes eggs
    find-links =
        http://dist.plone.org/release/4.3-latest/
        http://dist.plone.org/thirdparty/

    newest = false
    show-picked-versions = true

    # Agregar paquetes eggs adicionales para Plone aquí
    eggs =
        plone.app.workflowmanager
        Products.DCWorkflowGraph
        aws.zope2zcmldoc
        collective.loremipsum
        redturtle.maps.portlet
        Products.PloneBoard
        collective.subscribemember
        eea.facetednavigationtaxonomiccheckbox
        wildcard.media
        collective.portlet.twitter
        collective.cover

    # Activar la inicialización de ZCML de los paquetes que lo requieran
    # e.g. zcml = my.package my.other.package
    zcml =
    versions = versions

    parts = instance
        lxml
        zopepy

    [versions]
    Products.PloneBoard = 3.4
    redturtle.maps.portlet = 0.0.1.1
    collective.subscribemember = 1.11
    eea.facetednavigationtaxonomiccheckbox = 0.1.1
    wildcard.media = 1.0b1
    collective.portlet.twitter = 1.0b3

    collective.cover = 1.0a9
    plone.app.blocks = 1.1.1
    plone.app.drafts = 1.0a2
    plone.app.tiles = 1.0.1
    plone.formwidget.namedfile = 1.0.10
    plone.tiles = 1.2

    Products.DCWorkflowGraph = 0.4.1

    # Esta receta inicializa e instala la instancia de Zope 2.
    [instance]
    recipe = plone.recipe.zope2instance
    user = admin:admin
    http-address = 8080

    # Para agregar productos Plone se requiere utiliza ${buildout:eggs} 
    # en la declarativa eggs. Es posible referirse con esta sintaxis a 
    # cualquier variable de una de las partes, así: ${parte:variable} 
    eggs =
        Plone
        plone.app.upgrade
        ${buildout:eggs}
    zcml = ${buildout:zcml}
    enviroment-vars = 
        zope_i18n_compile_mo_files true

    [lxml]
    recipe = z3c.recipe.staticlxml
    egg = lxml

    # Interpreté de IPython generado con todos los paquetes activados 
    # en el PYTHONPATH, incluso los eggs de esta instalación
    [zopepy]
    recipe = zc.recipe.egg
    eggs = ${instance:eggs}
    interpreter = zopepy
    scripts = zopepy

En los comentarios en el código se explican las secciones del buildout.

Artículos relacionados
======================

.. seealso::

    * Artículos sobre :ref:`replicación de proyectos Python <python_buildout>`.

    * Vídeo tutorial llamado `Using buildout to install Zope and Plone`_ en Ingles, 
      creado por WebLion at Penn State, activistas de `PloneEdu.org`_.

    .. raw:: html

        <p>Vídeo tutorial llamado <b>Using buildout to install Zope and Plone</b> en Ingles</p>
        
        <div style="margin-top:10px;" align="center">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/zZBE0uu5pGg" frameborder="0" allowfullscreen></iframe>
        </div>
        
        <p>creado por WebLion at Penn State, activistas de <a href="http://ploneedu.org" target="_target" title="PloneEdu"><b>PloneEdu.org</a></b></p>


Referencias
===========

- `¿Qué es buildout?`_ desde la comunidad Plone México.

- `Replicación de proyectos Python`_ desde la comunidad Plone Venezuela.

.. _¿Qué es buildout?: http://www.plone.mx/docs/buildout.html
.. _Replicación de proyectos Python: https://plone-spanish-docs.readthedocs.io/es/latest/buildout/replicacion_proyectos_python.html
.. _ZODB: https://plone-spanish-docs.readthedocs.io/es/latest/zope/zodb/index.html#que-es-zodb
.. _paquete Setuptools: https://plone-spanish-docs.readthedocs.io/es/latest/python/setuptools.html#que_es_setuptools
.. _pip: https://plone-spanish-docs.readthedocs.io/es/latest/python/distribute_pip.html#que-es-pip
.. _modos de instalación de entornos virtuales de Python: https://plone-spanish-docs.readthedocs.io/es/latest/python/creacion_entornos_virtuales.html#instalacion-virtualenv
.. _modos de instalación de setuptools y easy_install: https://plone-spanish-docs.readthedocs.io/es/latest/python/setuptools.html#instalacion-easyinstall
.. _entornos virtuales de Python: https://plone-spanish-docs.readthedocs.io/es/latest/python/creacion_entornos_virtuales.html#creacion-entornos-virtuales
.. _paster: https://plone-spanish-docs.readthedocs.io/es/latest/python/skel_proyectos_python.html#que-es-pastescript
.. _Using buildout to install Zope and Plone: https://www.youtube.com/embed/zZBE0uu5pGg
.. _PloneEdu.org: https://twitter.com/ploneedu