.. -*- coding: utf-8 -*-

.. _introduccion:

============
Introducción
============

Esta documentación le enseñará a realizar diversas modos de instalación y aplicar 
sus configuraciones.

.. _modos_instalacion:

Modos de instalación
====================

Tradicionalmente las aplicaciones basadas en Linux, Apache, MySQL y PHP - LAMP requiere 
instalar un servidor Web y sus módulos, servidor de base de datos, lenguaje de aplicación, 
etc, cada uno se instala y configura por separado.

Para el servidor Zope / Plone no es muy distinto a las instalaciones LAMP, (Ver Figura 1.2), 
Más en cambio con Plone tiene un ventaja competitiva vs. LAMP la cual le puede construir, 
instalar y configurar de forma casi automática un servidor Web y sus módulos, servidor de 
base de datos, lenguaje de aplicación y sus módulos, etc; gracias a la herramienta :term:`buildout`.

.. figure:: _static/lamp_vs_zope_plone.png
  :align: center
  :alt: Stack de instalación de LAMP y Plone

  Stack de instalación de LAMP y Plone.


.. _Plone system requirements: https://docs.plone.org/4/en/manage/installing/requirements.html
.. _plone.org: https://plone.com/providers.html
.. _Hosting providers from plone.net website: http://plone.org/support/hosting-providers
.. _escala: https://docs.plone.org/manage/docker/docs/scaling/index.html
.. _servidor ZEO: https://zope.readthedocs.io/en/latest/zopebook/ZEO.html
.. _Requerimientos de sistema de Plone: http://www.coactivate.org/projects/ploneve/~xad~xadrequerimientos-de-sistema-de-plone
