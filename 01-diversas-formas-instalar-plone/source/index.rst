.. -*- coding: utf-8 -*-

.. Diversas formas de instalar Plone documentation master file, created by
   sphinx-quickstart on Sun Aug 24 21:56:27 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Diversas formas de instalar Plone
=================================

**Sobre este documento**

Es documento es un compendio de varios artículos publicados previamente que hablan sobre el
servidor de aplicaciones Zope y posibles configuraciones de despliegue de aplicación.

Esta compendio de artículos toca generalidades del servidor Zope como su instalación y configuración
para aplicaciones basadas en Plone CMS.

Plone esta basado en el `servidor de aplicaciones Zope <https://plone-spanish-docs.readthedocs.io/es/latest/zope/index.html>`_ 
y este requiere realizar tareas de hospedaje y administrativa para un servidor de aplicación 
Zope / sitio de Plone.

.. toctree::
    :maxdepth: 2
 
    introduccion
    requerimientos
    instalando
    vagrant
    buildout/plone4
    primeros_pasos/index
    productos/index
    content_well_portlets/index
    dropdown_menu/index
