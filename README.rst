.. -*- coding: utf-8 -*-

=======================================
Entrenamiento "Diseño Web en Plone CMS"
=======================================

Repositorio de manuales y recursos del entrenamiento "Administración de Infraestructura de servicios Plone"
realizado por la empresa `Covantec R.L. <https://github.com/Covantec>`_

.. contents :: :local:

Estructura general
===================

La estructura general de contenidos esta confirmada por los principales archivos:

**00-capacitacion-sysadmin-plone.odt**
  Describe el contenido del entrenamiento.

**01-diversas-formas-instalar-plone**
  Describe los contenidos del módulo *1* del entrenamiento.

**02-0-virtualenv.odp**
  Describe los contenidos del módulo *2* del entrenamiento.

**02-1-sistema-integracion-buildout.odp**
  Describe los contenidos del módulo *2* del entrenamiento.

**03-servidor-zope**
  Describe los contenidos del módulo *3* del entrenamiento.

**04-0-seguridad-en-plone.odp**
  Describe los contenidos del módulo *4* del entrenamiento.

**04-1-usuario-grupos-roles.odp**
  Describe los contenidos del módulo *4* del entrenamiento.

**04-2-flujo-trabajo-politicas.odp**
  Describe los contenidos del módulo *4* del entrenamiento.

**05-actualizaciones-en-plone.odp**
  Describe los contenidos del módulo *5* del entrenamiento.

**06-gestion-zodb**
  Describe los contenidos del módulo *6* del entrenamiento.

**07-conectores-almacenamiento-zodb.odp**
  Describe los contenidos del módulo *7* del entrenamiento.

**08-integracion-rdbms**
  Describe los contenidos del módulo *8* del entrenamiento.

**09-relstorage**
  Describe los contenidos del módulo *9* del entrenamiento.

**10-modos-autenticacion-usuarios**
  Describe las laminas del módulo *10* del entrenamiento.

**11-0-optimizacion-rendimiento-servicios.odp**
  Describe los contenidos del módulo *11* del entrenamiento.

**11-1-optimizacion-rendimiento-servicios**
  Describe los contenidos del módulo *11* del entrenamiento.

**12-politica-cache-objetos**
  Describe los contenidos del módulo *12* del entrenamiento.


Obtener y compilar la documentación
===================================

El almacenamiento de este material está disponible en un repositorio Git 
en "`entrenamiento.infraestructura_plone`_". Si usted tiene una
credenciales en este servidor y desea convertirse en un colaborador ejecute 
el siguiente comando: ::

  $ git clone https://bitbucket.org/covantec/entrenamiento.sysadmin_plone.git

Crear entorno virtual de Python para reconstruir este proyecto: ::

  $ sudo apt-get install python-pip python-setuptools git
  $ sudo apt-get install texlive-latex-base texlive-latex-recommended texlive-lang-spanish
  $ sudo pip install virtualenv
  $ cd $HOME ; mkdir $HOME/virtualenv ; cd $HOME/virtualenv
  $ virtualenv venv
  $ source $HOME/virtualenv/venv/bin/activate

Instale Sphinx: ::

  (venv)$ pip install Sphinx

Módulo 1
--------
  
Ahora puede generar la documentación en PDF del módulo *1*; ejecute los siguientes comando: ::

  (venv)$ cd entrenamiento.sysadmin_plone/01-diversas-formas-instalar-plone
  (venv)$ make latexpdf

Una vez generado el archivo PDF se puede abrir desde el directorio ``build/latex/01-diversas-formas-instalar-plone.pdf``
con cualquiera de sus programas de visor de PDF favorito (Evince, Acrobat Reader, etc).

Módulo 3
--------
  
Ahora puede generar la documentación en PDF del módulo *3*; ejecute los siguientes comando: ::

  (venv)$ cd entrenamiento.sysadmin_plone/03-servidor-zope
  (venv)$ make latexpdf

Una vez generado el archivo PDF se puede abrir desde el directorio ``build/latex/03-servidor-zope.pdf`` con cualquiera
de sus programas de visor de PDF favorito (Evince, Acrobat Reader, etc).

Módulo 6
--------

Ahora puede generar la documentación en PDF del módulo *6*; ejecute los siguientes comando: ::

  (venv)$ cd entrenamiento.sysadmin_plone/06-gestion-zodb
  (venv)$ make latexpdf

Una vez generado el archivo PDF se puede abrir desde el directorio ``build/latex/06-gestion-zodb.pdf`` con cualquiera de sus
programas de visor de PDF favorito (Evince, Acrobat Reader, etc).

Módulo 8
--------

Ahora puede generar la documentación en PDF del módulo *8*; ejecute los siguientes comando: ::

  (venv)$ cd entrenamiento.sysadmin_plone/08-integracion-rdbms
  (venv)$ make latexpdf

Una vez generado el archivo PDF se puede abrir desde el directorio ``build/latex/08-integracion-rdbms.pdf`` con cualquiera de sus
programas de visor de PDF favorito (Evince, Acrobat Reader, etc).

Módulo 9
--------

Ahora puede generar la documentación en PDF del módulo *9*; ejecute los siguientes comando: ::

  (venv)$ cd entrenamiento.sysadmin_plone/09-relstorage
  (venv)$ make latexpdf

Una vez generado el archivo PDF se puede abrir desde el directorio ``build/latex/09-relstorage.pdf`` con cualquiera de sus
programas de visor de PDF favorito (Evince, Acrobat Reader, etc).

Módulo 10
---------

Ahora puede generar la documentación en PDF del módulo *10*; ejecute los siguientes comando: ::

  (venv)$ cd entrenamiento.sysadmin_plone/10-modos-autenticacion-usuarios
  (venv)$ make latexpdf

Una vez generado el archivo PDF se puede abrir desde el directorio ``build/latex/10-modos-autenticacion-usuarios.pdf`` con
cualquiera de sus programas de visor de PDF favorito (Evince, Acrobat Reader, etc).

Módulo 11
---------

Ahora puede generar la documentación en PDF del módulo *11*; ejecute los siguientes comando: ::

  (venv)$ cd entrenamiento.sysadmin_plone/11-1-optimizacion-rendimiento-servicios
  (venv)$ make latexpdf

Una vez generado el archivo PDF se puede abrir desde el directorio ``build/latex/11-1-optimizacion-rendimiento-servicios.pdf`` con
cualquiera de sus programas de visor de PDF favorito (Evince, Acrobat Reader, etc).

Módulo 12
---------

Ahora puede generar la documentación en PDF del módulo *12*; ejecute los siguientes comando: ::

  (venv)$ cd entrenamiento.sysadmin_plone/12-politica-cache-objetos
  (venv)$ make latexpdf

Una vez generado el archivo PDF se puede abrir desde el directorio ``build/latex/12-politica-cache-objetos.pdf`` con
cualquiera de sus programas de visor de PDF favorito (Evince, Acrobat Reader, etc).

Colabora
========

¿Tiene alguna idea?, ¿Encontró un error? Por favor, hágalo saber registrando un `ticket de soporte`_.

.. _entrenamiento.infraestructura_plone: https://bitbucket.org/covantec/entrenamiento.sysadmin_plone
.. _ticket de soporte: https://bitbucket.org/covantec/entrenamiento.sysadmin_plone/issues/new
