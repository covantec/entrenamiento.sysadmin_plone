.. -*- coding: utf-8 -*-

.. Zope Object Database documentation master file, created by
   sphinx-quickstart on Sun Aug 24 21:56:27 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Zope Object Database
====================

.. toctree::
    :maxdepth: 2
 
    zodb
    puntos_montaje_db
    importar_exportar_data
    respaldar
    compactar
    actualizar_catalog
    reparar

.. 
  Apéndices
  =========

  .. toctree::
      :maxdepth: 2
 
      buildout/replicacion_proyectos_python
      buildout/programar_tareas_crontab
      glosario

      Analysing Data.fs content offline <https://plone.org/documentation/kb-old/debug-zodb-bloat>
